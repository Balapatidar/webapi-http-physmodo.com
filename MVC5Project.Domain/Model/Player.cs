//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVC5Project.Domain.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Player
    {
        public int PlayerId { get; set; }
        public Nullable<int> OrgId { get; set; }
        public string PlayerFirstName { get; set; }
        public string PlayerLastName { get; set; }
        public Nullable<System.DateTime> OrgStartDate { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string PlayerPhoneNumber { get; set; }
        public string PlayerEmailAddress { get; set; }
        public string PlayerNumber { get; set; }
        public string PlayerPicture { get; set; }
    }
}
