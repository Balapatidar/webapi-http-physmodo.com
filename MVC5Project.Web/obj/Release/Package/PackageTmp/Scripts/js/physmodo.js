(function(){
  var scrollTime = 1.3;
  var scrollDistance = 200;
  var iPadAgent = navigator.userAgent.match(/iPad/i) != null;
  var iPodAgent = navigator.userAgent.match(/iPhone/i) != null;
  var AndroidAgent = navigator.userAgent.match(/Android/i) != null;
  var webOSAgent = navigator.userAgent.match(/webOS/i) != null;
  var init = false;
  $(document).ready(function(){
    $(window).on("load", function() {
     addSwitchText();
      $(".loadingBlock").css("display", "none");
      $(".doneBlock").css("opacity", 1);
    });
  }); 
  $(document).mousemove(function(event){
    var mountainSmallX = 15;
    var mountainSmallY = 7.5;
    var winHeight = $(window).height();
    var winWidth = $(window).width();
    var yDiff = (event.pageY - (winHeight/2)) / mountainSmallY;
    var xDiff = (event.pageX - (winWidth/2)) / mountainSmallX;
    $(".mountain-range").css("left", xDiff * 1.25);
    $(".device").css("left", xDiff/1.15);
    $(".mountain-shadows").css("left", xDiff/1);
  })
  /////SCENE 1
  function guySweating(controller){
    var tween = new TimelineMax();
    var tweenArrays = [];
     for(var i=1;i<5;i++){
      var randTime = Math.random() * 2 + 1;
      tweenArrays.push(TweenMax.from('.sweat-droplet' + i, randTime / 2, {opacity: "-=1", yoyo:true, repeat: -1}));
      tweenArrays.push(TweenMax.to('.sweat-droplet' + i, randTime, {bottom: "-=" + (50 * randTime) + "px", ease: Power2.easeIn, repeat: -1}));
    }
    tween.add(tweenArrays);
    var scene = new ScrollScene({triggerElement: "#trigger2", triggerHook: 1, duration: $(window).height()*2})
      .on("start", function(e){
        if(e.scrollDirection === "FORWARD"){
          $(".top-nav-bar").addClass("fixed");
          $(".top-nav-bar").css("top", -$(".top-nav-bar").height() + "px");
          TweenMax.to(".top-nav-bar", ".5", {top: 0, overwrite: true});
          getNavLeft("[href='#the-problem']");
        }
      })
      .on("enter", function(){
        tween.play();
      })
      .on("leave", function(e){
        if(e.scrollDirection === "REVERSE"){
          TweenMax.to(".top-nav-bar", ".5", {top: -$(".top-nav-bar").height() + "px",  overwrite: true, onComplete: function(){
            $(".top-nav-bar").removeClass("fixed");
            $(".top-nav-bar").css("top", 0);
            $("#hover-arrow").data("origLeft", 0);
            $("[href='#the-problem']").mouseover();
          }});
        }
        tween.stop()
      })
			.addTo(controller);
   var tween2 = new TimelineMax()
   .add(TweenMax.to(".ridges", 3, {bottom:"-80px"}));
    var scene2 = new ScrollScene({triggerElement: "#trigger2", triggerHook: 1, duration:$(window).height()/1.5, offset:250})
   
	.setTween(tween2)
	.addTo(controller);
  }
  
 function tumblingRocksMan(controller){
    var timeline = new TimelineMax();
    var tween1 = TweenMax.from(".bg3-container .circle.1", 1, {css: {opacity:"0"}, delay:2})
    var tween2 = TweenMax.from(".bg3-container .circle.2", 1, {css: {opacity:"0"}, delay:0.5})
    var tween3 = TweenMax.from(".bg3-container .circle.3", 1, {css: {opacity:"0"}, delay:0.5})
     var tween4 = new TimelineMax()
       .add([
						TweenMax.to(".slice3", 16, {bottom: "-50rem", ease: Power2.easeInOut}),
           TweenMax.to(".side-rock", 16, {bottom: "-80rem", ease:  Power1.easeInOut}),
            //TweenMax.to(".falling-dude", 1, {rotation:60, right: "20%", bottom: "-82%"}),
            TweenMax.to(".falling-dude", 16, {rotation:75, bottom: "-50rem", ease: Power2.easeIn, delay: 1}),
           TweenMax.fromTo(".tumbling-rocks-layer1", 16, {bottom: "20rem"}, {bottom: "-50rem", ease: Power1.easeInOut, rotation:-20}),
         TweenMax.fromTo(".tumbling-rocks-layer2", 16, {bottom: "20rem"}, {bottom: "-50rem", ease: Power1.easeInOut, rotation:10})
					])
     
/*var tween5 = new TimelineMax()
       .add([
         
					])*/
     timeline
       .add(tween1)
       .add(tween2)
       .add(tween3)
    var scene = new ScrollScene({triggerElement: "#trigger3", triggerHook: 1, duration:$(".bg3-container").height()/2});
		scene.setTween(timeline);
		scene.addTo(controller);
    var scene2 = new ScrollScene({triggerElement: "#trigger3", duration:$(window).height()*1.5, triggerHook: ".3", pushFollowers: false})
						.setTween(tween4)
						.addTo(controller);
   /*var scene3 = new ScrollScene({triggerElement: "#trigger3", duration:$(window).height()*3.5, offset: 100, triggerHook: ".3", pushFollowers: false})
						.setTween(tween5)
						.addTo(controller);*/
  }
  function sadManInCave(controller){
    var timeline = new TimelineMax();
    var tween1 = TweenMax.from(".rays-layer1", 1, {opacity: "-=.5", yoyo:true, repeat: -1, ease: Cubic.easeIn})
    var tween2 = TweenMax.from(".rays-layer2", 1, {opacity: "-=.6", yoyo:true, repeat: -1, delay: .8})
    var tween3 = TweenMax.fromTo(".there-is-a-solution-gradient", 1.5, {opacity:'0'}, {opacity:'.7', repeat:-1, yoyo: true})
    //var tween3 = TweenMax.from(".rays-layer1", 5, {repeat: -1, rotation: 360})
    //var tween4 = TweenMax.from(".rays-layer2", 5, {repeat: -1, rotation: 360})
     timeline
       .add([tween1, tween2, tween3]);
    var scene = new ScrollScene({triggerElement: "#trigger4", triggerHook: 1, duration: $(window).height()*2, pushFollowers: false})
      .on("enter", function(){
        timeline.play();
        getNavLeft("[href='#the-solution']");
      })
      .on("leave", function(e){
        if(e.scrollDirection === "REVERSE"){
           getNavLeft("[href='#the-problem']");
        }
        timeline.stop()
      })
			scene.addTo(controller);
  }
  
 function howItWorksBubbles(controller) {
   var tween1 = new TimelineMax()
					.add(
						TweenMax.from(".bg7-container .circle.1", 1, {opacity:"0"})
					);
   var tween2 = new TimelineMax()
					.add(
						TweenMax.from(".bg7a-container .circle.2", 1, {opacity:"0"})
					);
   var tween3 = new TimelineMax()
					.add(
						TweenMax.from(".bg7b-container .circle.3", 1, {opacity:"0"})
					);
   var scene1 = new ScrollScene({triggerElement: "#trigger5", triggerHook: 1,         duration:$(window).height()/1.5})
   
	 .setTween(tween1)
	 .addTo(controller)
   .on("enter", function(){
     getNavLeft("[href='#how-it-works']");
   })
   .on("leave", function(e){
     if(e.scrollDirection === "REVERSE"){
       getNavLeft("[href='#the-app']");
     }
   })
   var scene2 = new ScrollScene({triggerElement: "#trigger6", triggerHook: 1,         duration:$(window).height()/1.5})
   
	.setTween(tween2)
	.addTo(controller);
   var scene3 = new ScrollScene({triggerElement: "#trigger7", triggerHook: 1,         duration:$(window).height()/1.5})
   
	.setTween(tween3)
	.addTo(controller);
 }
 function solutionCave(controller){
   var tween1 = new TimelineMax()
					.add([
            TweenMax.to(".glow", 4, {height:"48rem", top:"22rem", ease: Power1.easeInOut}),
            TweenMax.to(".cave-rock", 6, {left: "13rem"})            
					]);
   var scene = new ScrollScene({triggerElement: "#trigger14", triggerHook: 1, offset:50, duration: $(window).height()})
      .setTween(tween1)
			.addTo(controller);
 }
  
 function inroducingTheApp(controller) {
   var tween1 = new TimelineMax()
					.add([
						TweenMax.from(".more-precise", 1, {opacity:"0"}),
            TweenMax.from(".precise-data-capturing", 1, {scale: .2, opacity: "0"}),
            TweenMax.from(".scientists-can-be-scientists", 1, {scale: .2, opacity: "0", delay:.5}),
            TweenMax.from(".scientists", 1, {opacity: "0", delay:.5})
					]);
   var tween2 = new TimelineMax()
					.add([
						TweenMax.from(".saves-time-text", 1, {opacity:"-=1"}),
            TweenMax.from(".saves-time", 1, {scale: .2, opacity: "-=1"}),
            TweenMax.from(".brings-all-devices", 1, {scale: .2, opacity: "-=1", delay:.5}),
            TweenMax.from(".brings-together", 1, {opacity: "-=1", delay:.5})
					]);
   var tween3 = new TimelineMax()
         .add(
           TweenMax.from(".other-device", 6, {bottom: "-250px", opactiy: "-=1"})
         );
   var tween4 = new TimelineMax()
         .add(
           TweenMax.from(".app-requirements", 3, {opacity: "-=1"})
         );
   var scene1 = new ScrollScene({triggerElement: "#trigger8", triggerHook: 1, duration:$(window).height()/1.5})
	.setTween(tween1)
	.addTo(controller)
   .on("enter", function(){
     getNavLeft("[href='#the-app']");
   })
   .on("leave", function(e){
     if(e.scrollDirection === "REVERSE"){
       getNavLeft("[href='#the-solution']");
     }
   })
   var scene2 = new ScrollScene({triggerElement: "#trigger9", triggerHook: 1, duration:$(window).height()/1.5})
   
	.setTween(tween2)
	.addTo(controller);
   var scene3 = new ScrollScene({triggerElement: "#trigger10", triggerHook: 1, duration:$(window).height()/1.5})
   
   .setTween(tween3)
   .addTo(controller)
   var scene4 = new ScrollScene({triggerElement: "#trigger11", triggerHook: 1, duration:$(window).height()/1.5})
   
   .setTween(tween4)
   .addTo(controller)
 }
  
  function setupScrolling(){
        if(iPadAgent || iPodAgent || AndroidAgent || webOSAgent){
      var controller = new ScrollMagic({container: "#scroll_wrapper"});
      $('#scroll_wrapper').height($(window).height());
      $('#scroll_wrapper').width($(window).width()); 
    }
    else{
      $(window).on("mousewheel DOMMouseScroll", function(event){
          
        event.preventDefault();	
                  
        var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
        var scrollTop = $('#scroll_wrapper').scrollTop();
        var finalScroll = scrollTop - parseInt(delta*scrollDistance);
        
        TweenMax.to($('#scroll_wrapper'), scrollTime, {
          scrollTo : { y: finalScroll, autoKill:true },
              ease: Power1.easeOut,
              overwrite: 5
          });
      });
      }
  }
  function parallaxPin(scene, pin){
   scene.on("progress", function (e) {
     if(iPadAgent || iPodAgent || AndroidAgent || webOSAgent){ 
       $(pin).css('top', ($(pin).css("top")/2));
     }
     else{
       $(pin).css('top', ($(pin).css("top")/2));
     }
   });
}

function introAnim(controller){
  var physIntroAnim = new TimelineMax({delay: 0.5, onComplete: function(){
      var movePinAnim = new TimelineMax();
      movePinAnim.add([
        TweenMax.fromTo($('.mountain-range'), 3, {css: {"bottom":"40px"}}, {css: {"bottom":"-40px"}, ease: Power1.easeIn}),
        TweenMax.fromTo($('.mountain-shadows'), 3, {css: {"bottom":"40px"}}, {css: {"bottom":"-100px"}, ease: Power1.easeIn})
      ])
      var scene = new ScrollScene({triggerElement: "#trigger1", triggerHook: 1, duration: $(window).height()/5.5, pushFollowers: false})
      .setTween(movePinAnim)
      .setPin("#pin1")
      .addTo(controller)
      parallaxPin(scene, ".bg2-container");
    }});
      physIntroAnim.add([
        TweenMax.to( $('.device'), 1.2, {opacity: 1, ease: Power2.easeOut}),
        TweenMax.to( $('.device'), 1.2, {"bottom": 0, ease: Bounce.bounceOut}),
        TweenMax.to( $('.mountain-range'), 1.75, {"bottom": 53, ease: Power2.easeOut}),
        TweenMax.to( $('.mountain-shadows'), 1.8, {"bottom": 53, ease: Power2.easeOut})
      ])
    physIntroAnim.play();
}
function successfullMan(controller){
  var tween = new TimelineMax()
         .add([
           TweenMax.to(".clouds", 20, {right: "-15px", repeat:-1, yoyo:true, ease: Power1.easeInOut}),
           TweenMax.from(".clouds", 5, {opacity:"-=.5", repeat:-1, yoyo:true})
         ]);
  var tween2 = new TimelineMax()
  .add(TweenMax.to(".clouds", 10, {bottom:"60vh"}))
  var scene = new ScrollScene({triggerElement: "#trigger13", triggerHook: 1, duration: $(window).height()*2, pushFollowers: false})
      .on("enter", function(){
        tween.play()
      })
      .on("leave", function(e){
        tween.stop()
      })
      .setTween(tween2)
			.addTo(controller);
  }
  function getNavLeft(el){
    var elz = $(el).parent();
    var newWidth = $(elz).width()/2 - 10;
    var newLeft = $(elz).position().left + newWidth;
    $("#hover-arrow").data("origLeft", newLeft);
    $(elz).mouseover();
  }
function mountainScene(controller){
  var tween;
  var scene = new ScrollScene({triggerElement: "#trigger1"})
              .setTween(tween)
              .addTo(controller);
              scene.addIndicators();
}
function secondScene(controller){
  var tween;
      var scene = new ScrollScene({triggerElement: "#trigger2"})
              .setTween(tween)
              .addTo(controller);
      scene.addIndicators();
}

function downArrow(controller){
   var tween = new TimelineMax({repeat: -1});
  TweenMax.to(".bg1-bottom", 1, {opacity: 1, ease: Power2.easeIn});
  tween.add([
    TweenMax.from(".arrow-down", 2, {top: "-5.5rem", ease: Power2.easeOut}),
    TweenMax.to(".arrow-down", 1, {opacity: 1, ease: Power2.easeIn})
  ]);
  tween.play();
}
var switchText = {
  physical: [
    {sel: "#today", txt: "We rely on our physicians and surgeons to heal the stress, the over-doing, and the pushing we do to maintain our busy lifestyles."},
    {sel: "#canOnlyH2", txt: "Physicians can only do so much."},
    {sel: "#canOnly", txt:"As a result, we are left with a heap of problems in the industry."},
    {sel: ".increased-p", txt:"Increased patient volume"},
    {sel: ".rising-p", txt:"Rising insurance premiums"},
    {sel: ".prescribed-p", txt:"Patients not completing prescribed therapy"},
    {sel: "#solution", txt:"Now there is a tool to bridge the gap between patients, doctors and even insurance companies."},
    {sel: "#physSolution", txt:"Now there is a tool to bridge the gap between patients, doctors and even insurance companies."},
    {sel: "#continuum", txt:"Physmodo offers a convenient and powerful end-to-end digital continuum of care for modern physical therapy clinics."},
    {sel: ".brings-together", txt:"Brings together all devices."},
    {sel: "#measureP", txt:"The physician uses the technology to assess and analyze the patient's range of motion, presciribing the most ideal and accurate therapy plan based on real scientific data."},
    {sel: "#monitorP", txt:"The patient is guided through the therapy plan, while being closely monitored and tracked, and wirelessly reposrting back real-time feedback to the care phyisician."},
    {sel: "#differenceP", txt:"Patient outcomes are improved, naturally and efficiently, with better results. These results encourage the patient to continue the therapy, therefore healing quickly."}        
  ],
 sports: [
    {sel: "#today", txt: "Today’s athletes push their bodies to the limit. Even with support from trainers and team doctors, injuries are still on the rise in almost every major sport."},
   {sel: "#todayH2", txt: "In today's world, we are not always gentle on our bodies."},
    {sel: "#canOnlyH2", txt:"Athletes now are forced to play with injuries."},
   {sel: "#canOnly", txt:"As a result, we are left with a heap of problems in the industry."},
    {sel: ".increased-p", txt:"An icepack becomes a surgery "},
   {sel: ".rising-p", txt:"Strains and sprains turn into tears"},
    {sel: ".prescribed-p", txt:"Untreated aches can lead to sitting the bench for weeks"},
    {sel: "#solution", txt:"There is a tool that bridges the gap between science and technology, between performing and not performing."},
   {sel: "#physSolution", txt:"Its our mission to help in the care of athletes around the world by utilizing the latest technologies, to keep athletes performing at their best."},
    {sel: "#continuum", txt:"Physmodo offers a convenient and powerful platform, built on the principles of Sports Science."},
    {sel: ".brings-together", txt:"Brings data that you have never seen before to your fingertips."},
    {sel:"#measureP", txt:"Sports Scientist use our technology to assess and analyze the athletes performance levels off the field. Painting an amazing picture of an athletes overall performance capabilities."},
    {sel: "#monitorP", txt:"The athlete is guided through activity plans that you create, keeping the power and expertise in your hands, all while giving you real time feedback and analytics."},
    {sel: "#differenceP", txt:"Performance outcomes come naturally with our system. Keeping the athletes playing and making a difference when it really matters."}    
  ]
}
  
function addSwitchText(){
  $("[data-action='climb-up']").click(function(e){
    /*$(".scroll_wrapper").scrollTop(0);*/
    e.preventDefault();
      var idScroll = $(e.currentTarget).attr("href");
       
       TweenMax.to(".scroll_wrapper", 2, {scrollTo:{y: 0}, ease:Power2.easeOut});
  })
  $("[data-action='switchText']").click(function(e){
    var textType = $(e.currentTarget).attr("data-target");
    $.each(switchText[textType], function(index, textBlock){
      $(textBlock.sel).html(textBlock.txt);
    })
  })
  $("[data-target='sports']").click();
   if(!init){
    var controller = new ScrollMagic({container: "#scroll_wrapper", loglevel: 3}); 
     setupScrolling();
     $(".scroll_wrapper").css("overflow-y", "auto");
     $(".bg1-container").removeClass("bg1-intro");
     $("[data-action='scroll-to']").click(function(e){
      e.preventDefault();
      var idScroll = $(e.currentTarget).attr("href");
       var thisValue = $(idScroll).offset().top + $(".scroll_wrapper").scrollTop();
       TweenMax.to(".scroll_wrapper", 2, {scrollTo:{y:thisValue}, ease:Power2.easeOut});
    });
     downArrow(controller);
     introAnim(controller);
     guySweating(controller);
     tumblingRocksMan(controller);
     sadManInCave(controller);
     howItWorksBubbles(controller);
     inroducingTheApp(controller)
     randomTestimonial(controller);
     successfullMan(controller);
     solutionCave(controller);
     init = true;
   }
}
  var testimonials = [
    {txt: "Physmodo is so great.", by: "Paul Echols"},
    {txt: "I love physmodo!!", by: "Every Doctor"},
    {txt: "Physmodo keeps my patients happy.", by: "Dr. Doctor"},
    {txt: "Physmodo will change the way we do sports science", by: "Every Coach"}
  ];
  function randText(){
    var randomText = testimonials[Math.floor(Math.random()*testimonials.length)];
    $(".speech-bubble .test").html('"' + randomText.txt + '"');
    $(".speech-bubble .title").html("-" + randomText.by);
  }
  function randTestPos(){
    return (Math.random() * 55) + 17 + "%";
  }
function randomTestimonial(controller){
  //test and contact
  var tween = new TimelineMax();
  randText();
  var i = 0;
  tween.add(
    TweenMax.from('.speech-bubble', 0.2, {opacity: "0", scale: ".7", yoyo:true, repeat: -1, ease: Power2.easeInOut, repeatDelay: 2, onRepeat: function(){
      i++;
      if(i % 2 === 0) {
        randText();
        $(".speech-bubble").css("left",randTestPos);
        tween.repeatDelay(2);
      }
      else{
        tween.repeatDelay(0);
      }
    }})
  )
  var scene = new ScrollScene({triggerElement: "#trigger12", triggerHook: 1, duration:$(window).height()*2})
  .on("enter", function(){
    tween.play();
    getNavLeft("[href='#the-inspiration']");
  })
  .on("leave", function(e){
    tween.stop()
    if(e.scrollDirection === "REVERSE"){
      getNavLeft("[href='#how-it-works']")
    }
  })
  .addTo(controller)
  var scene2 = new ScrollScene({triggerElement: "#contact", triggerHook: 1, duration: $(window).height()*2})
  .on("enter", function(){
    getNavLeft("[href='#contact']");
  })
  .on("leave", function(){
    if(e.scrollDirection === "REVERSE"){
      getNavLeft("[href='#the-inspiration']");
    }
  })
  .addTo(controller);
}
})();

$(function() {
    var $el, leftPos, newWidth,
        $mainNav = $("#listings");
    
    $mainNav.append("<li id='hover-arrow'></li>");
    var $magicLine = $("#hover-arrow");
    $magicLine
        //.css("left", $(".current_page_item").position().left)
        .data("origLeft", $magicLine.position().left)
    $("#listings li").hover(function() {
        $el = $(this);
        newWidth = $el.width()/2 - 10;
        leftPos = $el.position().left + newWidth;
        $magicLine.stop().animate({
            left: leftPos
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft")
        });    
    });
$(function() { 
  $('.btn-primary').click(function() {
     var nextActive = $(this);
    $('.btn-primary').removeClass('active');
     $(nextActive).addClass('active');
     });
  });
});