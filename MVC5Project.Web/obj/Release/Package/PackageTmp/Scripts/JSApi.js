﻿function getAllPlayer1()
{
    $.ajax({
       // url: '@Url.Action("getAllTeamPlayer", "Team")',
        url: "/Team/getAllTeamPlayer",
        type: 'POST',
        data: JSON.stringify({ orgId: 10 }), 
        success: function (data) {
            alert("Company: " + data);
        },
        error: function (req, status, error) {
            alert("R: " + req + " S: " + status + " E: " + error);
        }
    });
   

}



var myApp = angular.module('ManageApp', []);
myApp.controller('ApiDataController', function ($scope, $http) {
    $scope.exerciseInit = function () {
        $http.get("/ApiData/GetAllExcercise").success(function (response) {

            $scope.exercies = response;
           
            console.log($scope.players);

        })
            .error(function () { alert("error"); })
    }
    $scope.playersinit = function ()
    {
        var orgId = $("#OrgId").val();
        $http.get("/ApiData/getAllTeamPlayer?orgId=" + orgId).success(function (response) {
            $scope.players = response;           
            console.log($scope.players);
        })
                   .error(function () { alert("error"); })
    }
    $scope.playerExerciseinit = function ()
    {
        var playerId = $("#PlayerId").val();
        $http.get("/ApiData/GetPlayerExercises?playerId=" + playerId).success(function (response) {

            $scope.exercises = response;
           
            console.log($scope.exercises);

        })
   .error(function () { alert("error"); })
    }

    $scope.allExBatchesInit = function () {
        $http.get("/ApiData/ExercisesBatches")
        .success(function (result) {
            $scope.batches = result;

            console.log($scope.batches);
        })
        .error(function () { alert("error"); })
    }
    $scope.DataOrginit = function () {
        var userName = $("#txtUsername").val();
        $http.get("/ApiData/DataOrganization?userName=" + userName)
        .success(function(response){
            $scope.orgDetail = response;           
            
            console.log($scope.orgDetail);
        })
        .error(function () {
            alert("error");
        })
    }
    $scope.postExerciseinit = function () {
        var exId = $("#ExerciseID").val();
        var exName = $("#ExerciseName").val();
        var exStatus = $("#selectStatus").val();
        $http.get("/ApiData/postEx?exId=" + exId + "&exName=" + exName + "&status" + exStatus)
        .success(function(){
            alert("Exercise is successfully posted");
        })
        .error(function () {
            alert("error");
        })
    }
    $scope.GetExerciseID = function () {
        var exId = $("#txtExerciseId").val();
        $http.get("/ApiData/ExeById?exId=" + exId)
        .success(function (response) {
            $scope.exer = response;
            console.log($scope.exer);
        })
        .error(function () {
            alert("error");
        })
    }
    $scope.DelExerciseID = function () {
        var exId = $("#txtExerciseId").val();
        $http.get("/ApiData/delEx?exId=" + exId)
        .success(function (response) {          
            console.log(response);
            alert("Exercise is successfully deleted");
        })
        .error(function () {
            alert("error");
        })
    }
    $scope.GetPlayerMeasurement = function () {
        var orgId = $("#txtOrgId").val();
        var exId = $("#txtExerciseId").val();
        var batchId = $("#txtBatchId").val();
        $http.get("/ApiData/getPlayerMeasurement?orgId=" + orgId + "&exId=" + exId + "&batchId=" + batchId)
       .success(function (response) {
           $scope.plyrMsrs = response;
           console.log(response);           
       })
       .error(function () {
           alert("error");
       })
    }
    $scope.putExercise = function () {
        var id = $("#txtId").val();
        var exId = $("#txtExId").val();
        var exName = $("#txtExname").val();
        var exStatus = $("#ddlStatus").val();
        $http({
            method: 'Post',
            url: '/ApiData/PutExById',
            data: {id:id,exId:exId,exName:exName,active:exStatus}
        })           
        .success(function () {
            alert("Done Successfully");
        })
        .error(function () {
            alert("Oops some error");
        })
    }
    $scope.GetPlayer = function () {
        var fname = $("#txtFirstname").val();
        var lName = $("#txtLastname").val();
        var orgid = $("#txtOrgId").val();
        $http({
            method: 'Post',
            url: '/ApiData/GetPlayerById',
            data: { fName: fname, lName: lName, OrgId: orgid }
        })
        .success(function (response) {
            $scope.Player = response;
            console.log(response);
        })
        .error(function () { alert("Oops some error");})
    }

    $scope.GetPlayerScore = function () {
        var playerId = $("#txtPlayerId").val();
        $http.get("/ApiData/GetPlScores?playerId=" + playerId)
        .success(function (result) {
            $scope.plDetail = result;
            console.log(result);
        })
        .error(function () {
            alert("Oops some error");
        })
    }
    $scope.PutPlayerScore = function () {
        var ScoreId = $("#txtScoreId").val();
        var PlayerId = $("#txtPlayerId").val();
        var OrgId = $("#txtOrgId").val();
        var ExerciseName = $("#txtExerciseName").val();
        var ExerciseCount = $("#txtExerciseCount").val();
        var ExerciseSide = $("#txtExerciseSide").val();
        var ExerciseTime = $("#txtExerciseTime").val();
        var Score = $("#txtScore").val();
       // var Datetime = new Date();
        var Datetime = $("#txtDatetime").val();
        $http({
            method: 'Post',
            url: '/ApiData/PutPScore',
            data: { ScoreId: ScoreId, PlayerId: PlayerId, OrgId: OrgId, ExerciseName: ExerciseName, ExerciseCount: ExerciseCount, ExerciseSide: ExerciseSide, ExerciseTime: ExerciseTime, Score: Score, Datetime: Datetime }
        })
        .success(function () {
            alert("Done");
        })
        .error("Error");
    }


    $scope.PutPlayerMotion = function () {

        var MotionId = $("#txtScoreId").val();
        var PlayerId = $("#txtPlayerId").val();
        var OrgId = $("#txtOrgId").val();
        var FileTimestamp = $("#txtFileTimestamp").val();
        var XmlStorageLocation = $("#txtXmlStorageLocation").val();
        var StatusFlag = $("#txtStatusFlag").val();
        var ProcessedFlag = $("#ProcessedFlag").val();       
        
        $http({
            method: 'Post',
            url: '/ApiData/putPlayerRange',
            data: { MotionId: MotionId, PlayerId: PlayerId, OrgId: OrgId, FileTimestamp: FileTimestamp, XmlStorageLocation: XmlStorageLocation, StatusFlag: StatusFlag, ProcessedFlag: ProcessedFlag }
        })
        .success(function () {
            alert("Done");
        })
        .error("Error");
    }

    $scope.GetUserId = function () {

        var userName = $("#txtUserName").val(); 
        $http({
            method: 'Get',
            url: '/ApiData/UserId?userName=' + userName
           
        })
        .success(function (response) {
            $scope.UserDetail = response;
            console.log(response);
        })
        .error("Error");
    }
    $scope.GetAzureStorage = function () {

        var storageID = $("#txtstorageID").val();
        $http({
            method: 'Get',
            url: '/ApiData/AzureStorage?storageID=' + storageID

        })
        .success(function (response) {
            $scope.storageDetail = response;
            console.log(response);
        })
        .error("Error");
    }

    $scope.GetOrgName = function () {

        var orgID = $("#txtOrgId").val();
        $http({
            method: 'Get',
            url: '/ApiData/OrgName?orgID=' + orgID

        })
        .success(function (response) {
            $scope.orgDetail = response;
            console.log(response);
        })
        .error("Error");
    }

    $scope.GetTrainerId = function () {

        var userName = $("#txtUserName").val();
        $http({
            method: 'Get',
            url: '/ApiData/GetTrainer?userName=' + userName

        })
        .success(function (response) {
            $scope.trnDetail = response;
            console.log(response);
        })
        .error("Error");
    }

    $scope.dataGet = function () {

        var id = $("#txtId").val();
        $http({
            method: 'Get',
            url: '/ApiData/GetDat?id=' + id

        })
        .success(function (response) {
            $scope.dtDetail = response;
            console.log(response);
        })
        .error("Error");
    }
    $scope.UserInfo = function () {

       
        $http({
            method: 'Get',
            url: '/ApiData/inFo'

        })
        .success(function (response) {
            $scope.UInfo = response;
            console.log(response);
        })
        .error("Error");
    }
    $scope.register = function () {
        var Email = $("#txtEmail").val();
        var Password = $("#txtPwd").val();
        var ConfirmPassword = $("#txtPwd1").val();
       // alert(Email);
        $http({
            method: 'Get',
            url: '/ApiData/reg',
            params: { uname: Email,pwd: Password , pwd1: ConfirmPassword }
        })
        .success(function (response) {
            $scope.msg=response
            alert(response);
            console.log(response);
        })
        .error("Error");
    }
    $scope.login = function () {
        var userName = $("#txtUserName").val();
        var pwd = $("#txtPwd").val();
        var confirmPwd = $("#txtPwd1").val();
        $http({
            method: 'Get',
            url: '/ApiData/loginApi',
            params: { userName: userName, userPwd: pwd}
        })
        .success(function (result) {
            if (result == "True") { alert("true"); }
            else { alert(result);}
           // window.open("AllExercises/AllExercises");
        })
        .error(function () {
            alert("Oops some error!");
        })
    }

    $scope.ManageInfo = function () {
        var returnurl = $("#txtReturnUrl").val();
        var genState = $("#selGenstate").val();
        $http({
            method: 'Get',
            url: '/ApiData/mInfo',
            params: {returnUrl:returnurl,genState:genState}
        })
        .success(function(response)
        {
            $scope.manage = response;
            console.log($scope.manage);
            //alert("done");
        })
        .error(function () {
            alert("Error");
        })
    }

    $scope.passwordChange = function () {
        var oldPwd = $("#txtOldPwd").val();
        var newPwd = $("#txtNewPwd").val();
        var confirmPwd = $("#txtCnfrmPwd").val();
        $http({
            method: 'Get',
            url: '/ApiData/chngPwd',
            params: { oldPwd: oldPwd, NewPwd: newPwd,ConfirmPwd:confirmPwd }
        })
        .success(function (response) {
            
            console.log(response);
            if (response == "True")
            { alert("Changed"); $("#txtOldPwd").val(''); $("#txtNewPwd").val(''); $("#txtCnfrmPwd").val(''); }
            else { alert("Some Error!"); }
        })
        .error(function () {
            alert("Error");
        })
    }
    $scope.setPassword = function () {
      
        var newPwd = $("#txtNewPwd").val();
        var confirmPwd = $("#txtCnfrmPwd").val();
        $http({
            method: 'Get',
            url: '/ApiData/setPwd',
            params: {  NewPassword: newPwd,ConfirmPassword:confirmPwd }
        })
        .success(function (response) {
            
            console.log(response);
            if (response == "True")
            { alert("Changed");  $("#txtNewPwd").val(''); $("#txtCnfrmPwd").val(''); }
            else { alert("Already has a password set!"); }
        })
        .error(function () {
            alert("Error");
        })
    }
    $scope.RegisterExternal = function () {
        var Email = $("#txtEmail").val();       
        $http({
            method: 'Get',
            url: '/ApiData/regExternal',
            params: { Email: Email}
        })
        .success(function (response) {
            console.log(response);           
            alert("Registered Successfully"); $("#txtEmail").val('');           
        })
        .error(function () {
            alert("Error");
        })
    }
    $scope.dataPut = function () {
        var Id = $("#txtId").val();
        var View = $("#txtView").val();
        var Name = $("#txtName").val();
        var PlayerId = $("#txtPlayerId").val();
        var OrgID = $("#txtOrgID").val();
        var ExerciseBatchId = $("#txtExerciseBatchId").val();
        var ExerciseID = $("#txtExerciseID").val();
        var ExerciseValue = $("#txtExerciseValue").val();
        var OrganizationName = $("#txtOrganizationName").val();
        var ExerciseName = $("#txtExerciseName").val();
        var dt = $("#hiddate").val();
        $http({
            method: 'Get',
            url: '/ApiData/pData',
            params: { Id: Id, View: View, Name: Name, PlayerId: PlayerId, OrgID: OrgID, ExerciseBatchId: ExerciseBatchId, ExerciseID: ExerciseID, ExerciseValue: ExerciseValue, OrganizationName: OrganizationName, ExerciseName: ExerciseName ,Datetime:dt}
        })
        .success(function (response) {
            console.log(response);
            alert("Done"); $("#txtId").val('');
        })
        .error(function () {
            alert("Error");
        })
    }

    $scope.getvalueinit = function () {
        $http({
            method: 'Get',
            url: '/ApiData/GetVal'
            
        })
        .success(function (response) {
            $scope.valu = response;
            console.log(response);
           // alert("Done"); 
        })
        .error(function () {
            alert("Error");
        })
    }
    $scope.valId = function () {
        var id = $("#txtId").val();
        $http({
            method: 'Get',
            url: '/ApiData/valId',
            params:{id:id}
        })
               .success(function (response) {
                   $scope.valu = response;
                   console.log(response);
                   // alert("Done"); 
               })
               .error(function () {
                   alert("Error");
               })
    }
    $scope.postval = function () {
        var id = $("#txtId").val();
        $http({
            method: 'Get',
            url: '/ApiData/postVal',
            params: { id: id }
        })
               .success(function (response) {
                 
                   console.log(response);
                   alert("Done"); 
               })
               .error(function () {
                   alert("Error");
               })
    }
    $scope.putval = function () {

        var id = $("#txtId").val();
        var string = $("#txtString").val();
        $http({
            method: 'Get',
            url: '/ApiData/putVal',
            params: { id: id, str: string }
        })
               .success(function (response) {
                   console.log(response);
                   alert("Done");
               })
               .error(function () {
                   alert("Error");
               })
    }
    $scope.delval = function ()
    {
        var id = $("#txtId").val();
       
        $http({
            method: 'Get',
            url: '/ApiData/delVal?id=' + id

        })
               .success(function (response) {
                   console.log(response);
                   alert("Successful");
               })
               .error(function (err) {
                   console.log(err);
                   alert("Error");
               })
    }

    $scope.removeLogin = function () {
        var Loginprovider = $("#txtLoginprovider").val();
        var ProviderKey = $("#txtProviderKey").val();
        $http({
            method: 'Get',
            url: '/ApiData/rmLogin',
            params: {LoginProvider:Loginprovider,ProviderKey:ProviderKey}
        })
               .success(function (response) {
                   console.log(response);
                   alert("Successful");
               })
               .error(function (err) {
                   console.log(err);
                   alert("Error");
               })
    }
});


