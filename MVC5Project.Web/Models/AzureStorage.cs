﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class AzureStorage
    {
        public int ID { get; set; }
        public string accountName { get; set; }
        public string primaryKey { get; set; }
        public string secondaryKey { get; set; }
    }
}