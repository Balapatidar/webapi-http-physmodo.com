﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class ExcesisesViewModel
    {
        [Required]
        [Display(Name = "Excercise")]
        public int ExerciseID { get; set; }

      
        public string ExerciseName { get; set; }
        public Nullable<bool> Active { get; set; }
        //  public string ExerciseName { get; set; }
    }
}