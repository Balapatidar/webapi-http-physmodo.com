﻿using MVC5Project.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class PlayerModel
    {
        public int PlayerId { get; set; }
        [Editable(false)]
        public string Name { get; set; }
        public string Title { get; set; }
        public IList<ExcerciseTypeViewModel> Excercises { get; set; }

        [Display(Name = "Customer Types")]
        public IList<ExcerciseTypeViewModel> AvailableExcercises { get; set; }
        [Required(ErrorMessage = "Select at least one Excercise type.")]
        public List<string> PostedExcercises { get; set; }
        public void LoadEditData()
        {
            this.AvailableExcercises = ExcerciseTypeViewModel.GetAll();
            if (this.PostedExcercises != null)
            {
                this.Excercises = ExcerciseTypeViewModel.GetAll().Where(c => PostedExcercises.Contains(c.ExcerciseId.ToString())).ToList();
            }
            else
            {
                this.Excercises = PlayerModel.GetCustomerTypeForCustomer(this.PlayerId);
            }
        }
        public static IList<ExcerciseTypeViewModel> GetCustomerTypeForCustomer(int customerId)
        {
            var types = new List<ExcerciseTypeViewModel>();
            var context = new DbMVC5ProjectEntities();
             types = (from e in context.Excercises
                         join pe in context.PlayerExcercises on e.ExerciseID equals pe.ExcerciseId
                         join p in context.Players on pe.PlayerId equals p.PlayerId where pe.PlayerId==customerId 
                      select new ExcerciseTypeViewModel {ExcerciseId=e.ExerciseID,ExcerciseName=e.ExerciseName}).ToList();
            //foreach (var item in hj)
            //{
            //     types.Add(new ExcerciseTypeViewModel { ExcerciseId = item.ExcerciseId, ExcerciseName = item.ExcerciseName });
            //}
            return types;
        }
        public static IList<PlayerModel> GetAll()
        {
            var context = new DbMVC5ProjectEntities();
            var hj = context.Players.ToList();

            var all = new List<PlayerModel>();
            foreach (var item in hj)
            {
                all.Add(new PlayerModel { PlayerId = item.PlayerId, Name = item.PlayerFirstName + " " + item.PlayerLastName });
            }
            return all;
        }
    }
}