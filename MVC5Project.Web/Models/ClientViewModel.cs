﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class ClientViewModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Organization")]
        public Nullable<int> OrgID { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        [Display(Name="State")]
        public int state { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        [Required]
        [Display(Name = "Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string primarycontact { get; set; }
        public string UserId { get; set; }


        //Properties for displaying data in Client Admin Section
        public string  combinename { get; set; }
    }
}