﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class PlayerScore
    {
        public int ScoreId { get; set; }
        public int PlayerId { get; set; }       
        public int OrgId { get; set; }
        public string ExerciseName { get; set; }
        public int ExerciseCount { get; set; }
        public string ExerciseSide { get; set; }
        public string ExerciseTime { get; set; }
        public int Score{get;set;}
        public DateTime Datetime { get; set; }
    }
}