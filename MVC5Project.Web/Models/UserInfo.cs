﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class UserInfo
    {
        public string Email { get; set; }
        public Boolean HasRegistered { get; set; }
        public string LoginProvider { get; set; }
    }
}