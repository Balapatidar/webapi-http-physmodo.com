﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class ActivityReportViewModel
    {
        public int Id { get; set; }
        public int ExerciseID { get; set; }
        public string ExerciseName { get; set; }
        public int PlayerId { get; set; }
        public string PlayerFirstName { get; set; }
        public string PlayerLastName { get; set; }
        public Nullable<System.DateTime> Datetime { get; set; }
        public Nullable<int> ExcerciseValue { get; set; }
        public string  FullName { get; set; }
    }
}