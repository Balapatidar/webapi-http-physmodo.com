﻿using MVC5Project.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class ExcerciseTypeViewModel
    {
        public int ExcerciseId { get; set; }
        public string ExcerciseName { get; set; }

        public static IList<ExcerciseTypeViewModel> GetAll()
        {
            var context = new DbMVC5ProjectEntities();
            var hj = context.Excercises.ToList();
            var all = new List<ExcerciseTypeViewModel>();
            foreach (var item in hj)
            {
                all.Add(new ExcerciseTypeViewModel { ExcerciseId = item.ExerciseID, ExcerciseName = item.ExerciseName });
            }
            return all;
        }
    }
}