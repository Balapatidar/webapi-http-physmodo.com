﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }
        public string Email { get; set; }
        public string Logins { get; set; }
        public string ExternalLoginProviders { get; set; }
        //public IList<UserLoginInfo> Logins { get; set; }
        //public IList<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
        
    }
}