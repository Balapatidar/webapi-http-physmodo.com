﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class PlayerInfoViewModel
    {
        [ScaffoldColumn(false)]
        public int PlayerId { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<int> OrgId { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string PlayerFirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string PlayerLastName { get; set; }
       
        public Nullable<System.DateTime> OrgStartDate { get; set; }
        [ScaffoldColumn(false)]
        public string DOB { get; set; }
        
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string PlayerPhoneNumber { get; set; }
        [Required]
        [Display(Name = "Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string PlayerEmailAddress { get; set; }
        public string PlayerNumber { get; set; }
        public string PlayerPicture { get; set; }
          [ScaffoldColumn(false)]
        public string CombineName { get; set; }
    }
}