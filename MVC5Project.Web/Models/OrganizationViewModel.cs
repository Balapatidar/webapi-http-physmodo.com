﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class OrganizationViewModel
    {
        public int OrgId { get; set; }
        [Required]
        [Display(Name = "Organization")]
        public string OrgName { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}