﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class RolesList
    {
        public string DisplayText { get; set; }
        public int Value { get; set; }
    }
}