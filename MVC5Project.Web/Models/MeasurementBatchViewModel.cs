﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class MeasurementBatchViewModel
    {
        public int Id { get; set; }
        public string View { get; set; }
        public string  Name { get; set; }
        public Nullable<int> PlayerId { get; set; }
        public Nullable<int> OrgID { get; set; }
        public Nullable<System.DateTime> Datetime { get; set; }
        public Nullable<int> ExcerciseBatchId { get; set; }
        public Nullable<int> ExcerciseID { get; set; }
        [Required]
        public Nullable<int> ExcerciseValue { get; set; }
        public string OrganizationName { get; set; }
        public string  ExcerciseName { get; set; }
    }
}