﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class RequestAccessViewModel
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string OrganizationName { get; set; }
        public string UserId { get; set; }
    }
}