﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC5Project.Web.Models
{
    public class ProfileViewModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name="First Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string firstname { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string lastname { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public int  state { get; set; }
        public string zip { get; set; }
        //[RegularExpression(@"^\d+$", ErrorMessage = "Please enter proper contact details.")]
        public string phone { get; set; }
        public string primarycontact { get; set; }
        public string UserId { get; set; }
        public string  picturepath { get; set; }
    }
}