﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;


namespace MVC5Project.Web.Code
{
    public class ApiRequests
    {
        //fetch a list of exercises   done
        public async Task<string> allExercises()
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/AllExercises");
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            // request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;

        }
        //fetch exercise batches by get method  done
        public async Task<string> ExercisesBatches()
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/ExerciseBatches");
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            // request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        //fetch user detail by passing username   done but result is in doubt
        public async Task<string> DataOrganization(string userName)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/orgnization?username=" + userName);
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            // request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        //post/insert exercise by post method  done
        public async Task<string> PostExercise(HttpContent content)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/PostExercise");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        //fetch a exercise by passing id  done
        public async Task<string> ExerciseById(string id)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetExercise?id=" + id);
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            // request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        //update exercise by passing id
        public async Task<string> PutExerciseById(string id, HttpContent content)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/PutExercise?id=" + id);
            request.Method = HttpMethod.Put;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PutAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            response.Content.Headers.Add("X-HTTP-Method-Override", "PUT");
            bool suc = response.IsSuccessStatusCode;
            return response.ToString();
        }



        public async Task<string> DeleteExercise(string id)
        {

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Delete;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/DeleteExercise?id=" + id);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.DeleteAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }

        public async Task<string> playerMesurements(string orgId, string exerciseId, string exerciseBatchId)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/playerMesurements?orgid=" + orgId + "&exerciseid=" + exerciseId + "&exerciseBatchid=" + exerciseBatchId);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;
        }
        public async Task<string> GetPlayer(string orgId, string firstName, string lastName)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetPlayer?firstName=" + firstName + "&lastName=" + lastName + "&OrgId=" + orgId);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;


        }
        public async Task<string> GetAllTeamPlayer(string orgId)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetAllTeamPlayers?OrgId="+orgId);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;

        
        }

       
        public async Task<string> GetPlayerExercises(string playerId)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetPlayerExercises?PlayerId=" + playerId );
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
           // response.EnsureSuccessStatusCode();
            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;

        }
        public async Task<string> GetPlayerScores(string playerId)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetPlayerScores?PlayerId=" + playerId);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;

        }

        public async Task<string> GetOrganizationName(string orgId)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetOrganizationName?orgID=" + orgId);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;

        }

        //fetch user detail by passing username   done but result is in doubt
        public async Task<string> GetTrainer(string userName)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetTrainerID?userName=" + userName);
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            // request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
         //   response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        public async Task<string> PutPlayerScore(HttpContent content)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Put;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/PutPlayerScore");
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PutAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;
        }

        public async Task<string> PutPlayerRangeOfMotion(HttpContent content)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Put;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/PutPlayerRangeOfMotion");
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PutAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;
        }
        public async Task<string> GetUserID(string userName)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetUserID?userName=" + userName);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string resBody = await response.Content.ReadAsStringAsync();
            return resBody;
        }
        public async Task<string> GetAzureStorage(string storageId)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/GetAzureStorage?storageID="+storageId);
            request.Method=HttpMethod.Get;
            HttpClient client=new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization=new AuthenticationHeaderValue("Bearer",AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response=await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType=new MediaTypeHeaderValue("application/xml");
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        public async Task<string> GETData(string Id)
        {
            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/" + Id);
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
            string responseBody = await response.Content.ReadAsStringAsync();
            return response.ToString();
        }
        public async Task<string> PutData(string Id,HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Data/" + Id);
            request.Method = HttpMethod.Put;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PutAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string responseBody = await response.Content.ReadAsStringAsync();
            return response.ToString();
        }

        public async Task<string> UserInfo()
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/UserInfo");
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string responseBody =await response.Content.ReadAsStringAsync();
            return responseBody;
        }

        public async Task<string> ChangePassword(HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/ChangePassword");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
           // string responseBody = await response.Content.ReadAsStringAsync();
            return response.StatusCode.ToString();
        }
        public async Task<string> Logout(HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/Logout");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string res = response.StatusCode.ToString();
           // string responseBody = await response.Content.ReadAsStringAsync();
            return res;
        }
        public async Task<string> ManageInfo(string returnUrl,string genState)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri=new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/ManageInfo?returnUrl="+returnUrl+"&generateState="+genState);
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.GetAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        public async Task<string> SetPassword(HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/SetPassword");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string responseBody = await response.Content.ReadAsStringAsync();
            return response.StatusCode.ToString();
           // return responseBody;
        }
        public async Task<string> Register(HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/Register");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri,content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            string responseBody = await response.Content.ReadAsStringAsync();
            return response.StatusCode.ToString() + responseBody;
           // return responseBody;
        }
        public async Task<string> RegisterExternal(HttpContent content)
        {
            
            string realm = "ApiData";
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/RegisterExternal");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue("Basic", "realm=\"" + realm + "\""));
                
            }
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> api_Values()
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Values");
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> api_Values_Id(string val)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Values/"+val);
            request.Method = HttpMethod.Get;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.SendAsync(request);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> Post_api_Values(HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Values");
            request.Method = HttpMethod.Post;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PostAsync(request.RequestUri,content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await response.Content.ReadAsStringAsync();
        }
        
    public async Task<string> PUT_api_Values(string Id,HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Values/"+Id);
            request.Method = HttpMethod.Put;
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.PutAsync(request.RequestUri, content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await response.Content.ReadAsStringAsync();
        } 
     public async Task<string> DELETE_api_Values(string Id)
        {
            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Delete;
            request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Values/"+Id);            
            HttpClient client = new HttpClient();
            AuthToken.connectHTTP();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
            HttpResponseMessage response = await client.DeleteAsync(request.RequestUri);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();

        }

     public async Task<string> RemoveLogin(HttpContent content)
     {
         var request = new HttpRequestMessage();
         request.Method = HttpMethod.Post;
         request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/RemoveLogin");
         HttpClient client = new HttpClient();
         AuthToken.connectHTTP();
         client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
         HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
         response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
         response.EnsureSuccessStatusCode();
         return await response.Content.ReadAsStringAsync();

     }
        //public async Task<string> Register(HttpContent data)
        //{
        //    var request = new HttpRequestMessage();enctype="multipart/form-data"
        //    request.RequestUri = new Uri("https://api-extranet-physmodo.azurewebsites.net/api/Account/Register");
        //    request.Method = HttpMethod.Get;
        //    HttpClient client = new HttpClient();
        //    AuthToken.connectHTTP();
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.access_token.ToString());
        //    // request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken.PhysmodoToken.ToString());
        //    HttpResponseMessage response = await client.PostAsync( (request, data);
        //    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //    response.EnsureSuccessStatusCode();
        //    string responseBody = await response.Content.ReadAsStringAsync();
        //    return responseBody;api/Data/PostExercise
        //}http://api-extranet-physmodo.azurewebsites.net/api/Data/ExerciseBatches

       
    }
}