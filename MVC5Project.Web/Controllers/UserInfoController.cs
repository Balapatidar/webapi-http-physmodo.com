﻿using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class UserInfoController : Controller
    {
        //
        // GET: /UserInfo/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserInfo()
        {
            return View();
        }
        public async Task<JsonResult> inFo()
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.UserInfo();          
            var tmp = new JavaScriptSerializer().Deserialize<UserInfo>(json);           
            return Json(tmp, JsonRequestBehavior.AllowGet);

          
        }
        
	}
}