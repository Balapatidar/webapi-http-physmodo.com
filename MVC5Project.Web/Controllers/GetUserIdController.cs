﻿using MVC5Project.Web.Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class GetUserIdController : Controller
    {
        //
        // GET: /GetUserId/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetUserId()
        {
            return View();
        }

        public async Task<JsonResult> UserId(string username)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetUserID(username);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
	}
}