﻿using MVC5Project.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class SetPasswordController : Controller
    {
        //
        // GET: /SetPassword/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SetPassword()
        {
            return View();
        }
        public async Task<bool> setPwd(string NewPassword, string ConfirmPassword)
        {
            bool flag = false;
            ApiRequests obj = new ApiRequests();
            var postData=new List<KeyValuePair<string,string>>();
            postData.Add(new KeyValuePair<string,string>("NewPassword",NewPassword));
            postData.Add(new KeyValuePair<string,string>("ConfirmPassword",ConfirmPassword));
            HttpContent content=new FormUrlEncodedContent(postData);
            string res = await obj.SetPassword(content);
            if (res == "OK") { flag = true; } else {
                flag = false; }
            return flag;

        }
	}
}