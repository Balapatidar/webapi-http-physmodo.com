﻿using MVC5Project.Domain.Model;
using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class ExerciseByIdController : Controller
    {
        //
        // GET: /ExerciseById/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetExercise()
        {
            return View();
        }

        public async Task<JsonResult> ExeById(string exId)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.ExerciseById(exId);
            var tmp = JsonConvert.DeserializeObject<ExcesisesViewModel>(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
	}
}