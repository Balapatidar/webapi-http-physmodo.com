﻿using MVC5Project.Web.Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class GetDataController : Controller
    {
        //
        // GET: /GetData/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetData()
        {
            return View();
        }
        public async Task<JsonResult> GetDat(string id)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GETData(id);
           // var tmp = JsonConvert.DeserializeObject(json);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
	}
}