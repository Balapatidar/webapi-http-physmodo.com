﻿using MVC5Project.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class DelExerciseController : Controller
    {
        //
        // GET: /DelExercise/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DeletExercise()
        { return View(); }

        public async Task<string> delEx(string exId)
        { 
             ApiRequests obj = new ApiRequests();
             string res = await obj.DeleteExercise(exId);
             return res;
        }
	}
}