﻿using MVC5Project.Domain.Model;
using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class ManageInfoController : Controller
    {
        //
        // GET: /ManageInfo/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ManageInfo()
        {
            return View();
        }
        public async Task<string> mInfo(string returnUrl, string genState)
        {
            ApiRequests obj = new ApiRequests();
           // IEnumerable<ManageInfoViewModel> Getlist = null;
         string res=await obj.ManageInfo(returnUrl, genState);
      // var Getlist = new JavaScriptSerializer().Serialize(res);
   //     var tmp = new JavaScriptSerializer().Deserialize<ManageInfoViewModel>(res);
        //return Json(res, JsonRequestBehavior.AllowGet);
         return res;
        }
	}
}