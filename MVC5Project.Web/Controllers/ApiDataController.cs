﻿using MVC5Project.Domain.Model;
using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class ApiDataController : Controller
    {
        //
        // GET: /ApiData/
        public ActionResult Index()
        {
            return View();
        }


        //login view
        public ActionResult Login()
        {
            return View();
        }
        //login code
        public bool loginApi(string userName, string userPwd)
        {
            bool rtnVal = false;
            rtnVal = AuthToken.connectHTTP(userName, userPwd);
            if (rtnVal == true)
            {
                Session["UserEmail"] = userName;
                Session["UserPwd"] = userPwd;
            }
            return (rtnVal);

            
        }
        //logout
        public async Task<ActionResult> logout(string userName, string userPwd)
        {
            //  bool rtnVal = false;
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("Email", Convert.ToString(Session["UserEmail"])));
            postData.Add(new KeyValuePair<string, string>("Password", Convert.ToString(Session["UserPwd"])));
            HttpContent content = new FormUrlEncodedContent(postData);
            ApiRequests objApi = new ApiRequests();
            string res = await objApi.Logout(content);
            if (res == "OK")
            {
                Session["UserEmail"] = null;
                Session["UserPwd"] = null;
                return RedirectToAction("Login", "ApiData");

            }
            else
            { //rtnVal = false;
                return RedirectToAction("Login", "ApiData");
            }


            // return Json(AuthToken.physmodoAccessToken);
        }

        //get user information
        public ActionResult UserInfo()
        {
            return View();
        }
        public async Task<JsonResult> inFo()
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.UserInfo();
            var tmp = new JavaScriptSerializer().Deserialize<UserInfo>(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);


        }
        //Get Manage Information
        public ActionResult ManageInfo()
        {
            return View();
        }
        public async Task<string> mInfo(string returnUrl, string genState)
        {
            ApiRequests obj = new ApiRequests();
            // IEnumerable<ManageInfoViewModel> Getlist = null;
            string res = await obj.ManageInfo(returnUrl, genState);
            // var Getlist = new JavaScriptSerializer().Serialize(res);
            //     var tmp = new JavaScriptSerializer().Deserialize<ManageInfoViewModel>(res);
            //return Json(res, JsonRequestBehavior.AllowGet);
            return res;
        }

        //register
        public ActionResult Register()
        {
            return View();
        }
        public async Task<string> reg(string uname, string pwd, string pwd1)
        {
            ApiRequests obj = new ApiRequests();
            var postdata = new List<KeyValuePair<string, string>>();
            postdata.Add(new KeyValuePair<string, string>("Email", uname));
            postdata.Add(new KeyValuePair<string, string>("Password", pwd));
            postdata.Add(new KeyValuePair<string, string>("ConfirmPassword", pwd1));
            HttpContent content = new FormUrlEncodedContent(postdata);
            string res = await obj.Register(content);
            return res;
        }
        //register External
        public ActionResult RegisterExternal()
        {
            return View();
        }
        public async Task<string> regExternal(string Email)
        {
            ApiRequests obj = new ApiRequests();
            var postdata = new List<KeyValuePair<string, string>>();
            postdata.Add(new KeyValuePair<string, string>("Email", Email));
            HttpContent content = new FormUrlEncodedContent(postdata);
            string res = await obj.RegisterExternal(content);
            return res;

        }

        //change password
        public ActionResult ChangePassword()
        {
            return View();
        }

        public async Task<bool> chngPwd(string oldPwd, string NewPwd, string ConfirmPwd)
        {
            bool flag = false;
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("OldPassword", oldPwd));
            postData.Add(new KeyValuePair<string, string>("NewPassword", NewPwd));
            postData.Add(new KeyValuePair<string, string>("ConfirmPassword", ConfirmPwd));
            HttpContent content = new FormUrlEncodedContent(postData);

            string result = await obj.ChangePassword(content);
            if (result == "OK") { flag = true; } else { flag = false; }
            return flag;
        }
        //set password

        public ActionResult SetPassword()
        {
            return View();
        }
        public async Task<bool> setPwd(string NewPassword, string ConfirmPassword)
        {
            bool flag = false;
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("NewPassword", NewPassword));
            postData.Add(new KeyValuePair<string, string>("ConfirmPassword", ConfirmPassword));
            HttpContent content = new FormUrlEncodedContent(postData);
            string res = await obj.SetPassword(content);
            if (res == "OK") { flag = true; }
            else
            {
                flag = false;
            }
            return flag;

        }

        //get all exercises
        public ActionResult AllExercises()
        {
            return View();
        }
        public async Task<JsonResult> GetAllExcercise()
        {
            ApiRequests obj = new ApiRequests();
            string token = Convert.ToString(Session["access_token"]);
            string newresponsefromserver = await obj.allExercises();
            var GettingAllexcercise = new JavaScriptSerializer().Deserialize<List<ExcesisesViewModel>>(newresponsefromserver);
            List<Excercise> listexcercise = new List<Excercise>();
            var Finallist = GettingAllexcercise.Select(c => new ExcesisesViewModel { ExerciseID = c.ExerciseID, ExerciseName = c.ExerciseName }).OrderByDescending(e => e.ExerciseID);
            return Json(Finallist, JsonRequestBehavior.AllowGet);
        }
        //all exercises batches
        public ActionResult AExercisesBatches()
        {
            return View();
        }

        public async Task<JsonResult> ExercisesBatches()
        {
            ApiRequests obj = new ApiRequests();
            string token = Convert.ToString(Session["access_token"]);
            string newresponsefromserver = await obj.ExercisesBatches();
            var GettingAllexcercise = new JavaScriptSerializer().Deserialize<List<ExcerciseBatchViewModel>>(newresponsefromserver);
            List<ExcerciseBatch> listexcercise = new List<ExcerciseBatch>();
            var Finallist = GettingAllexcercise.Select(c => new ExcerciseBatchViewModel { ExerciseBatchId = c.ExerciseBatchId, Datetime = c.Datetime }).OrderBy(e => e.ExerciseBatchId);
            return Json(Finallist, JsonRequestBehavior.AllowGet);

        }
        //data organization

        public ActionResult DataOrg()
        {
            return View();
        }

        public async Task<JsonResult> DataOrganization(string userName)
        {
            ApiRequests obj = new ApiRequests();
            //var context = new DbMVC5ProjectEntities();
            //  IEnumerable<OrganizationViewModel> GetOrganisation = null;
            string json = await obj.DataOrganization(userName);
            var tmp = JsonConvert.DeserializeObject(json);
            //var tmp = JsonConvert.DeserializeObject<OrganizationViewModel>(json);
            // var tmp = new JavaScriptSerializer().Deserialize<OrganizationViewModel>(json);           
            return Json(tmp, JsonRequestBehavior.AllowGet);          
        }

        //post exercise
        public ActionResult PostExercise()
        {
            return View();
        }
        public async Task<JsonResult> postEx(string exId, string exName, string status)
        {
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("ExerciseID", exId));
            postData.Add(new KeyValuePair<string, string>("ExerciseName", exName));
            postData.Add(new KeyValuePair<string, string>("Active", status));

            HttpContent content = new FormUrlEncodedContent(postData);
            ViewBag.result = await obj.PostExercise(content);
            return Json(ViewBag.result, JsonRequestBehavior.AllowGet);
        }

//get exercise
        public ActionResult GetExercise()
        {
            return View();
        }

        public async Task<JsonResult> ExeById(string exId)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.ExerciseById(exId);
            var tmp = JsonConvert.DeserializeObject<ExcesisesViewModel>(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }

        //put exercise
        public ActionResult PutExercise()
        { return View(); }

        public async Task<string> PutExById(string id, string exId, string exName, string active)
        {
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("ExerciseID", exId));
            postData.Add(new KeyValuePair<string, string>("ExerciseName", exName));
            postData.Add(new KeyValuePair<string, string>("Active", active));
            HttpContent content = new FormUrlEncodedContent(postData);
            string json = await obj.PutExerciseById(id, content);
            return json;

        }
        //delete exercise
        public ActionResult DeletExercise()
        { return View(); }

        public async Task<string> delEx(string exId)
        {
            ApiRequests obj = new ApiRequests();
            string res = await obj.DeleteExercise(exId);
            return res;
        }


        //player measurement
        public ActionResult PlayerMeasurement()
        {
            return View();
        }

        public async Task<JsonResult> getPlayerMeasurement(string orgId, string exId, string batchId)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.playerMesurements(orgId, exId, batchId);

            var GettingAllexcercise = new JavaScriptSerializer().Deserialize<List<MeasurementBatchViewModel>>(json);
            List<MeasurementBatchViewModel> listexcercise = new List<MeasurementBatchViewModel>();
            var Finallist = GettingAllexcercise.Select(c => new MeasurementBatchViewModel { Id = c.Id, Name = c.Name, PlayerId = c.PlayerId, OrgID = c.OrgID, Datetime = c.Datetime, ExcerciseBatchId = c.ExcerciseBatchId, ExcerciseID = c.ExcerciseID, ExcerciseValue = c.ExcerciseValue, OrganizationName = c.OrganizationName, ExcerciseName = c.ExcerciseName });
            return Json(Finallist, JsonRequestBehavior.AllowGet);

        }

        //get player
        public ActionResult GetPlayer()
        {
            return View();
        }
        public async Task<JsonResult> GetPlayerById(string fName, string lName, string OrgId)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetPlayer(OrgId, fName, lName);
            var Getplayer = new JavaScriptSerializer().DeserializeObject(json);
            // var Getplayer = JsonConvert.DeserializeObject(json);
            // var tmp = JsonConvert.DeserializeObject<ExcesisesViewModel>(json);
            return Json(Getplayer, JsonRequestBehavior.AllowGet);
        }

        //get all team player
        public ActionResult ViewPlayers()
        {
            return View();
        }
       // [HttpPost]
        public async Task<JsonResult> getAllTeamPlayer(string orgId)
        {           
            ApiRequests obj = new ApiRequests();
            //var context = new DbMVC5ProjectEntities();
            IEnumerable<PlayerInfoViewModel> GetAllPlayer = null;
            string newresponsefromserver = await obj.GetAllTeamPlayer(orgId);
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerInfoViewModel>>(newresponsefromserver);
            return Json(GetAllPlayer, JsonRequestBehavior.AllowGet);
           
        }

        //get player exercise
        public ActionResult PlayerExercises()
        {
            return View();
        }

        public async Task<JsonResult> GetPlayerExercises(string playerId)
        {
            ApiRequests obj = new ApiRequests();
            IEnumerable<PlayerExcercise> GetAllPlayer = null;
            string newresponsefromserver = await obj.GetPlayerExercises(playerId);
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerExcercise>>(newresponsefromserver);
            return Json(GetAllPlayer, JsonRequestBehavior.AllowGet);



        }
        //get player scoces

        public ActionResult GetPlayerScrores()
        {
            return View();
        }

        public async Task<JsonResult> GetPlScores(string playerId)
        {
            ApiRequests obj = new ApiRequests();
            IEnumerable<PlayerScore> GetAllPlayer = null;
            string json = await obj.GetPlayerScores(playerId);
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerScore>>(json);
            //return Json(GetAllPlayer, JsonRequestBehavior.AllowGet);
            // ApiRequests obj = new ApiRequests();
            string json1 = await obj.GetPlayerScores(playerId);
            var tmp = JsonConvert.DeserializeObject(json1);
            return Json(tmp, JsonRequestBehavior.AllowGet);


        }

        //put player score
        public ActionResult PutPlayerScore()
        {
            return View();
        }

        public async Task<string> PutPScore(string ScoreId, string PlayerId, string OrgId, string ExerciseName, string ExerciseCount, string ExerciseSide, string ExerciseTime, string Score, string Datetime)
        {
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("ScoreId", ScoreId));
            postData.Add(new KeyValuePair<string, string>("PlayerId", PlayerId));
            postData.Add(new KeyValuePair<string, string>("OrgId", OrgId));
            postData.Add(new KeyValuePair<string, string>("ExerciseName", ExerciseName));
            postData.Add(new KeyValuePair<string, string>("ExerciseCount", ExerciseCount));
            postData.Add(new KeyValuePair<string, string>("ExerciseSide", ExerciseSide));

            postData.Add(new KeyValuePair<string, string>("ExerciseTime", ExerciseTime));
            postData.Add(new KeyValuePair<string, string>("Score", Score));
            postData.Add(new KeyValuePair<string, string>("Datetime", Datetime));

            HttpContent content = new FormUrlEncodedContent(postData);
            string json = await obj.PutPlayerScore(content);
            return json;
        }
   
        //put player range of motion
        public ActionResult PutPlayerRangeOfMotion()
        {
            return View();
        }
        public async Task<string> putPlayerRange(string MotionId, string PlayerId, string OrgId, string FileTimestamp, string XmlStorageLocation, string StatusFlag, string ProcessedFlag)
        {
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("MotionId", MotionId));
            postData.Add(new KeyValuePair<string, string>("PlayerId", PlayerId));
            postData.Add(new KeyValuePair<string, string>("OrgId", OrgId));
            postData.Add(new KeyValuePair<string, string>("FileTimestamp", FileTimestamp));
            postData.Add(new KeyValuePair<string, string>("XmlStorageLocation", XmlStorageLocation));
            postData.Add(new KeyValuePair<string, string>("StatusFlag", StatusFlag));

            postData.Add(new KeyValuePair<string, string>("ProcessedFlag", ProcessedFlag));


            HttpContent content = new FormUrlEncodedContent(postData);
            string json = await obj.PutPlayerRangeOfMotion(content);
            return json;
        }
	
        //get user id
        public ActionResult GetUserId()
        {
            return View();
        }

        public async Task<JsonResult> UserId(string username)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetUserID(username);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
        //get azure storage
        public ActionResult GetAzureStorage()
        {
            return View();
        }
        public async Task<JsonResult> AzureStorage(string storageID)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetAzureStorage(storageID);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }

        //get organization name
        public ActionResult GetOrganizationName()
        {
            return View();
        }

        public async Task<JsonResult> OrgName(string orgID)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetOrganizationName(orgID);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }

        //get trainer id
        public ActionResult GetTrainerID()
        {
            return View();
        }


        public async Task<JsonResult> GetTrainer(string userName)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetTrainer(userName);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }


        //get data
        public ActionResult GetData()
        {
            return View();
        }
        public async Task<JsonResult> GetDat(string id)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GETData(id);
            // var tmp = JsonConvert.DeserializeObject(json);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        //put data
        public ActionResult PutData()
        {
            return View();
        }
        public async Task<string> pData(string Id, string View, string Name, string PlayerId, string OrgID, string ExerciseBatchId, string ExerciseID, string ExerciseValue, string OrganizationName, string ExerciseName, string date)
        {
            ApiRequests obj = new ApiRequests();
            var postdata = new List<KeyValuePair<string, string>>();
            postdata.Add(new KeyValuePair<string, string>("Id", Id));
            postdata.Add(new KeyValuePair<string, string>("View", View));
            postdata.Add(new KeyValuePair<string, string>("Name", Name));
            postdata.Add(new KeyValuePair<string, string>("PlayerId", PlayerId));
            postdata.Add(new KeyValuePair<string, string>("OrgID", OrgID));
            postdata.Add(new KeyValuePair<string, string>("ExerciseBatchId", ExerciseBatchId));
            postdata.Add(new KeyValuePair<string, string>("ExerciseID", ExerciseID));
            postdata.Add(new KeyValuePair<string, string>("ExerciseValue", ExerciseValue));
            postdata.Add(new KeyValuePair<string, string>("OrganizationName", OrganizationName));
            postdata.Add(new KeyValuePair<string, string>("ExerciseName", ExerciseName));
            postdata.Add(new KeyValuePair<string, string>("Datetime", date));
            HttpContent content = new FormUrlEncodedContent(postdata);
            string res = await obj.PutData(Id, content);
            return res;
        }
	
        //get values

        public ActionResult GetValues()
        {
            return View();
        }
        public async Task<string> GetVal()
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.api_Values();
            //var tmp = new JavaScriptSerializer().DeserializeObject(json);
            //  var tmp = JsonConvert.DeserializeObject(json);
            //string tmp = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return json;
        }

        //post values

        public ActionResult PostValue()
        {
            return View();
        }

        public async Task<string> postVal(string val)
        {
            ApiRequests obj = new ApiRequests();
            var postdata = new List<KeyValuePair<string, string>>();
            postdata.Add(new KeyValuePair<string, string>("string", val));
            HttpContent content = new FormUrlEncodedContent(postdata);
            string res = await obj.Post_api_Values(content);
            return res;
        }

        //put value

        public ActionResult PutValue()
        {
            return View();
        }
        public async Task<string> putVal(string id, string str)
        {
            ApiRequests obj = new ApiRequests();
            var postdata = new List<KeyValuePair<string, string>>();
            postdata.Add(new KeyValuePair<string, string>("string", str));
            HttpContent content = new FormUrlEncodedContent(postdata);
            string res = await obj.PUT_api_Values(id, content);
            return res;
        }

        //delete value
        public ActionResult DeleteValue()
        {
            return View();
        }
        public async Task<string> delVal(string id)
        {
            ApiRequests obj = new ApiRequests();
            string s = await obj.DELETE_api_Values(id);
            return s;
        }


        //remove login view
        public ActionResult RemoveLogin()
        {
            return View();
        }
        //remove login code
        public async Task<string> rmLogin(string LoginProvider, string ProviderKey)
        {
            ApiRequests obj = new ApiRequests();
             var postdata=new List<KeyValuePair<string,string>>();
             postdata.Add(new KeyValuePair<string, string>("LoginProvider", LoginProvider));
            postdata.Add(new KeyValuePair<string, string>("ProviderKey", ProviderKey));
                    HttpContent content = new FormUrlEncodedContent(postdata);
            string res=await obj.RemoveLogin(content);
            return res;
        }
	}
}