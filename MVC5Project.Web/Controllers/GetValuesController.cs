﻿using MVC5Project.Web.Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace MVC5Project.Web.Controllers
{
    public class GetValuesController : Controller
    {
        //
        // GET: /GetValues/
        public ActionResult Index()
        {
            return View();
        }
       public ActionResult GetValues()
        {
            return View();
        }
       public async Task<string> GetVal()
       {
           ApiRequests obj = new ApiRequests();
           string json = await obj.api_Values();
           //var tmp = new JavaScriptSerializer().DeserializeObject(json);
       //  var tmp = JsonConvert.DeserializeObject(json);
           //string tmp = Newtonsoft.Json.JsonConvert.SerializeObject(json);
           return json;
       }
	}
}