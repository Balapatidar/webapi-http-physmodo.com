﻿using MVC5Project.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class DeleteValueController : Controller
    {
        //
        // GET: /DeleteValue/
        public ActionResult Index()
        {
            return View();
        }
        public async Task<string> delVal(string id)
        {
            ApiRequests obj = new ApiRequests();
            string s = await obj.DELETE_api_Values(id);
            return s;
        }
	}
}