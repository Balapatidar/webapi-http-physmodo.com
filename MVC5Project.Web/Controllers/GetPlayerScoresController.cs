﻿using MVC5Project.Domain.Model;
using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class GetPlayerScoresController : Controller
    {
        //
        // GET: /GetPlayerScores/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetPlayerScrores()
        {
            return View();
        }

        public async Task<JsonResult> GetPlScores(string playerId)
        {
            ApiRequests obj = new ApiRequests();
            IEnumerable<PlayerScore> GetAllPlayer = null;
            string json = await obj.GetPlayerScores(playerId);
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerScore>>(json);
            //return Json(GetAllPlayer, JsonRequestBehavior.AllowGet);
           // ApiRequests obj = new ApiRequests();
            string json1 = await obj.GetPlayerScores(playerId);
            var tmp = JsonConvert.DeserializeObject(json1);
            return Json(tmp, JsonRequestBehavior.AllowGet);


        }
	}
}