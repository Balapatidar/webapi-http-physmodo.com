﻿using MVC5Project.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class PutPlayerScoreController : Controller
    {
        //
        // GET: /PutPlayerScore/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PutPlayerScore()
        {
            return View();
        }

        public async Task<string> PutPScore(string ScoreId, string PlayerId, string OrgId, string ExerciseName, string ExerciseCount, string ExerciseSide, string ExerciseTime, string Score, string Datetime)
        {
            ApiRequests obj = new ApiRequests();
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("ScoreId", ScoreId));
            postData.Add(new KeyValuePair<string, string>("PlayerId", PlayerId));
            postData.Add(new KeyValuePair<string, string>("OrgId", OrgId));
            postData.Add(new KeyValuePair<string, string>("ExerciseName", ExerciseName));
            postData.Add(new KeyValuePair<string, string>("ExerciseCount", ExerciseCount));
            postData.Add(new KeyValuePair<string, string>("ExerciseSide", ExerciseSide));

            postData.Add(new KeyValuePair<string, string>("ExerciseTime", ExerciseTime));
            postData.Add(new KeyValuePair<string, string>("Score", Score));
            postData.Add(new KeyValuePair<string, string>("Datetime", Datetime));
           
            HttpContent content = new FormUrlEncodedContent(postData);
            string json = await obj.PutPlayerScore(content);
            return json;
        }
    }
}