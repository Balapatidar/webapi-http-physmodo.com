﻿using MVC5Project.Domain.Model;
using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class PlayerMeasurementController : Controller
    {
        //
        // GET: /PlayerMeasurement/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PlayerMeasurement()
        {
            return View();
        }

        public async Task<JsonResult> getPlayerMeasurement(string orgId,string exId,string batchId)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.playerMesurements(orgId,exId,batchId);

            var GettingAllexcercise = new JavaScriptSerializer().Deserialize<List<MeasurementBatchViewModel>>(json);
            List<MeasurementBatchViewModel> listexcercise = new List<MeasurementBatchViewModel>();
            var Finallist = GettingAllexcercise.Select(c => new MeasurementBatchViewModel { Id = c.Id, Name = c.Name, PlayerId = c.PlayerId, OrgID = c.OrgID, Datetime = c.Datetime, ExcerciseBatchId = c.ExcerciseBatchId, ExcerciseID = c.ExcerciseID, ExcerciseValue = c.ExcerciseValue, OrganizationName = c.OrganizationName, ExcerciseName=c.ExcerciseName });
            return Json(Finallist, JsonRequestBehavior.AllowGet);

        }
	}
}