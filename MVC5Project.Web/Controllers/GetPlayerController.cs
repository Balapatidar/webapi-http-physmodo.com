﻿using MVC5Project.Web.Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class GetPlayerController : Controller
    {
        //
        // GET: /GetPlayer/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetPlayer()
        {
            return View();
        }
        public async Task<JsonResult> GetPlayerById(string fName, string lName, string OrgId)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetPlayer(OrgId, fName, lName);
            var Getplayer = new JavaScriptSerializer().DeserializeObject(json);
           // var Getplayer = JsonConvert.DeserializeObject(json);
           // var tmp = JsonConvert.DeserializeObject<ExcesisesViewModel>(json);
            return Json(Getplayer,JsonRequestBehavior.AllowGet);
        }
    }
}