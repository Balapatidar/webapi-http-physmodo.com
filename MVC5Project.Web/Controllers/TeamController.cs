﻿using System.Globalization;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MVC5Project.Domain.Model;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC5Project.Web.Models;
using System.Collections.Generic;
using System.IO;
using System.Data.Entity.Validation;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Kendo.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using MVC5Project.Web.Code;


namespace MVC5Project.Web.Controllers
{
   // [Authorize]
    [OutputCache(Duration = 0, NoStore = true, VaryByParam = "*")]
    public class TeamController : Controller
    {
        DbMVC5ProjectEntities context = new DbMVC5ProjectEntities();
        //private static readonly IList<PlayerModel> customers = PlayerModel.GetAll();
        private static readonly IList<PlayerModel> customers;
        public static int PlayerId = 0;

        public static int NewBatchId = 0;
        public static int NewExcerciseid = 0;
        public TeamController()
        {
        }
        public TeamController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        public ActionResult Index()
        {
            return this.View(customers);
        }

        public ActionResult LaunchManualProposalWindow(int customerId)
        {
            RouteValueDictionary routeValues = this.GridRouteValues();
            PlayerModel dd = new PlayerModel();
            dd.Excercises = PlayerModel.GetCustomerTypeForCustomer(customerId);
            dd.AvailableExcercises = ExcerciseTypeViewModel.GetAll();
            // return this.RedirectToAction("Index", routeValues);
          
            return  this.PartialView("~/Views/Team/EditorTemplates/Customer.cshtml",dd);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(PlayerModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                // update in the databse


                //Deleting First All Records By Playerid
                //    var getRecordByplayerId = context.PlayerExcercises.Where(p => p.PlayerId == model.PlayerId).ToList();
                //    foreach (var item in getRecordByplayerId)
                //    {
                //        PlayerExcercise pla = context.PlayerExcercises.Find(item.Id);
                //        if (pla != null)
                //        {
                //            context.Entry(pla).State = EntityState.Deleted;
                //            context.SaveChanges();
                //        }
                //    }
                //    foreach (var item in model.PostedExcercises)
                //    {
                //        int ExcerciseID = Convert.ToInt32(item);
                //        PlayerExcercise pe = new PlayerExcercise();
                //        pe.ExcerciseId = ExcerciseID;
                //        pe.PlayerId = model.PlayerId;
                //        context.Entry(pe).State = EntityState.Added;
                //        context.SaveChanges();

                //    }
                //}
                //RouteValueDictionary routeValues = this.GridRouteValues();
                //return this.RedirectToAction("Index", routeValues);

                //// return this.RedirectToAction("Index", routeValues);
                //ViewData["postedCustomerTypes"] = model.PostedExcercises;
            }
            return this.View("Index");
        }
        /// <summary>
        /// Action Result for adding a player in the system
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamPlayer(int? ID, string Message)
        {
            if (Request.QueryString["ID"] != null)
            {
                PlayerId = Convert.ToInt32(Request.QueryString["ID"]);
            }
            if (ID != null && ID != -1)
            {
                var orgid = context.UserDetails.FirstOrDefault(a => a.ID == ID);
                TempData["Message"] = Message;
                ViewBag.Orgid = orgid.OrgID;
            }
            else
            {
                var loggesdinUser = User.Identity.GetUserId();
                var GettingOrgnization = context.UserDetails.FirstOrDefault(a => a.UserId == loggesdinUser).OrgID;
                TempData["Message"] = Message;
                if (GettingOrgnization != null)
                {
                    ViewBag.Orgid = GettingOrgnization;
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult TeamPlayer(TeamViewModel model, int? hdnfilename)
        {

            Player oPlayer = new Player();
            string _finalPath = "";
            var _uploadImage = Request.Files[0];
            try
            {

                if (ModelState.IsValid)
                {  //Saveing the player Name to get PlayerID to save Player Image Within That Player'sID
                    oPlayer.PlayerFirstName = model.PlayerFirstName;
                    context.Players.Add(oPlayer);
                    context.SaveChanges();

                    //Getting PlayerID
                    var _playerResult = context.Players.Where(a => a.PlayerFirstName == model.PlayerFirstName).SingleOrDefault();
                    var _playerId = _playerResult.PlayerId;
                    oPlayer.OrgId = hdnfilename;
                    oPlayer.OrgStartDate = model.OrgStartDate;
                    oPlayer.PlayerEmailAddress = model.PlayerEmailAddress;

                    oPlayer.PlayerLastName = model.PlayerLastName;
                    oPlayer.PlayerNumber = model.PlayerNumber;
                    oPlayer.PlayerPhoneNumber = model.PlayerPhoneNumber;
                    oPlayer.DateOfBirth = model.DateOfBirth;
                    if (_uploadImage.ContentLength > 0)
                    {
                        string _fileExtention = Path.GetExtension(_uploadImage.FileName);
                        if (_fileExtention.ToLower().Contains(".jpg") || _fileExtention.ToLower().Contains(".png") || _fileExtention.ToLower().Contains(".gif") || _fileExtention.ToLower().Contains(".jpeg"))
                        {



                            //Making directory of Indvisual Player Based on their ID
                            string _fileName = Path.GetFileName(_uploadImage.FileName);
                            string _path = Server.MapPath("~/uploads/PlayerImages/" + _playerId + "/");
                            if (Directory.Exists(_path))
                            {

                                _uploadImage.SaveAs(_path + _fileName);
                            }
                            else
                            {
                                Directory.CreateDirectory(_path);
                                _uploadImage.SaveAs(_path + _fileName);
                            }
                            _finalPath = Path.Combine(_path, _fileName);

                            oPlayer.PlayerPicture = _playerId + "/" + _fileName;


                        }
                        else
                        {
                            TempData["Error"] = "You can only upload the Extention like .jpg, .png, .jpeg, .gif.";
                        }
                    }
                    context.Entry(oPlayer).State = EntityState.Modified;
                    context.SaveChanges();
                    ModelState.Clear();
                    model = null;
                    TempData["Message"] = "Player is saved successfully";
                    string Messa = TempData["Message"].ToString();
                    return RedirectToAction("TeamPlayer", new { ID = PlayerId, Message = Messa });
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            return View(model);
        }
        /// <summary>
        /// Page For Saving Athlete Detail
        /// </summary>
        /// <returns></returns>
        public ActionResult IndividualAtheleteProfile()
        {
            return View();
        }


        /// <summary>
        /// 20 October
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult Customers_Read([DataSourceRequest]DataSourceRequest request)
        {
            var loggedinUser = User.Identity.GetUserId();
            int? orgId = 0;
            string role = "";
            ModifyFiltersForCustomers(request.Filters);
            var GettingOrgnizationId = context.UserDetails.FirstOrDefault(a => a.UserId == loggedinUser).OrgID;
            if (GettingOrgnizationId != null)
            {
                orgId = GettingOrgnizationId;
            }
            if (User.IsInRole("System Admin"))
            {
                role = "System Admin";
            }
            else
            {
                role = "";
            }



            var result = context.Players.Select(a => new TeamViewModel { PlayerId = a.PlayerId, PlayerFirstName = a.PlayerFirstName, PlayerLastName = a.PlayerLastName, PlayerNumber = a.PlayerNumber, PlayerEmailAddress = a.PlayerEmailAddress, OrgId = a.OrgId }).ToList();
            //return Json(GetCustomers().ToDataSourceResult(request));
            string sortingorder = "";
            if (request.Sorts.Count != 0)
            {
                sortingorder = request.Sorts[0].SortDirection.ToString();
            }
            else
            {
                sortingorder = "Descending";
            }
            int ExcerciseBatchId = Convert.ToInt32(Session["ExcerciseBatchId"]);
            int ExcerciseId = Convert.ToInt32(Session["ExcerciseID"]);
            if (request.Filters.Count == 0)
            {
                NewBatchId = 0;
                NewExcerciseid = 0;
            }
            return Json(GetPlayerMesasuForRiskProfile(sortingorder, orgId, role, NewBatchId, NewExcerciseid).ToDataSourceResult(request));
        }

        private void ModifyFiltersForCustomers(IEnumerable<IFilterDescriptor> filters)
        {

            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "ExcerciseBatchId")
                    {
                        NewBatchId = Convert.ToInt32(descriptor.Value);

                        //  Session["ExcerciseBatchId"] = ExcerciseBatchId;

                    }
                    else if (descriptor != null && descriptor.Member == "ExcerciseID")
                    {
                        NewExcerciseid = Convert.ToInt32(descriptor.Value);

                        //  Session["ExcerciseID"] = ExcerciseID;
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {

                        ModifyFiltersForCustomers(((CompositeFilterDescriptor)filter).FilterDescriptors);
                    }
                }
            }
        }

        private static IEnumerable<TeamViewModel> GetPlayers()
        {
            var northwind = new DbMVC5ProjectEntities();

            return northwind.Players.Select(customer => new TeamViewModel
            {
                PlayerId = customer.PlayerId,
                PlayerFirstName = customer.PlayerFirstName,
                PlayerLastName = customer.PlayerLastName,
                PlayerNumber = customer.PlayerNumber,
                PlayerEmailAddress = customer.PlayerEmailAddress
            });
        }
        [HttpGet]
        public async Task<ActionResult> Measurement()
        {
            ApiRequests obj = new ApiRequests();
           // string token = Convert.ToString(Session["access_token"]);
            string newresponsefromserver = await obj.allExercises();
            var GettingAllexcercise = new JavaScriptSerializer().Deserialize<List<ExcesisesViewModel>>(newresponsefromserver);
            List<Excercise> listexcercise = new List<Excercise>();
            var Finallist = GettingAllexcercise.Select(c => new ExcesisesViewModel { ExerciseID = c.ExerciseID, ExerciseName = c.ExerciseName }).OrderBy(e => e.ExerciseID);
            //return Json(Finallist, JsonRequestBehavior.AllowGet);



            //var AllExcercise = context.Excercises.Where(a => a.Active == true).ToList();
            //Finallist.Insert(0, new Excercise { ExerciseID = -1, ExerciseName = "AllExcercises", Active = true });
            ViewBag.Excersices = new SelectList(Finallist, "ExerciseID", "ExerciseName");
            return View();
        }
        [HttpPost]
        public ActionResult Measurement(ExcesisesViewModel user, string ExerciseID)
        {
            IEnumerable<string> GetAllPlayerMesuremnts = new JavaScriptSerializer().Deserialize<List<string>>(ExerciseID);
            string url = Convert.ToString(Request.UrlReferrer);
            int index = url.LastIndexOf("=");
            url = url.Substring(index + 1);
            int PlayerID = Convert.ToInt32(url);
            if (user.ExerciseID != null)
            {
                var loggesdinUser = User.Identity.GetUserId();
                ExcerciseBatch exb = new ExcerciseBatch();
                exb.Datetime = DateTime.Now;
                exb.CreatedBy = loggesdinUser;
                context.Entry(exb).State = EntityState.Added;
                context.SaveChanges();
                var ExcerciseBatchId = exb.ExerciseBatchId;
                if (user.ExerciseID == -1)
                {
                    var GetOrganizationofLoggedinuser = context.UserDetails.FirstOrDefault(a => a.UserId == loggesdinUser).OrgID;
                    if (GetOrganizationofLoggedinuser != null)
                    {
                        //Collection Of Players
                        var GettingPlayersofOrganization = context.Players.Where(a => a.OrgId == GetOrganizationofLoggedinuser).ToList();
                        //Getting AllExcercise
                        var Allexcercise = context.Excercises.Where(a => a.Active == true).ToList();
                        foreach (var excer in Allexcercise)
                        {
                            if (GettingPlayersofOrganization.Count > 0)
                            {
                                foreach (var item in GettingPlayersofOrganization)
                                {
                                    PlayerMeasurement plme = new PlayerMeasurement();
                                    plme.PlayerId = item.PlayerId;
                                    plme.OrgID = item.OrgId;
                                    plme.ExcerciseBatchId = ExcerciseBatchId;
                                    plme.ExcerciseID = excer.ExerciseID;
                                    plme.ExcerciseValue = 0;
                                    plme.Datetime = DateTime.Now;
                                    context.Entry(plme).State = EntityState.Added;
                                    context.SaveChanges();
                                }

                            }
                            else
                            {
                                return Json(new { success = false, Message = "Measurement Is not done." });
                            }
                        }
                        return Json(new { success = true, Message = "Measurement has been done for all players" });

                    }
                }
                else
                {

                    var getRecordByplayerId = context.PlayerExcercises.Where(p => p.PlayerId == PlayerID).ToList();
                    foreach (var item in getRecordByplayerId)
                    {
                        PlayerExcercise pla = context.PlayerExcercises.Find(item.Id);
                        if (pla != null)
                        {
                            context.Entry(pla).State = EntityState.Deleted;
                            context.SaveChanges();
                        }
                    }
                    foreach (var item in GetAllPlayerMesuremnts)
                    {
                        int ExcerciseID = Convert.ToInt32(item);
                        PlayerExcercise pe = new PlayerExcercise();
                        pe.ExcerciseId = ExcerciseID;
                        pe.PlayerId = PlayerID;
                        context.Entry(pe).State = EntityState.Added;
                        context.SaveChanges();

                    }
                    var GetOrganizationofLoggedinuser = context.UserDetails.FirstOrDefault(a => a.UserId == loggesdinUser).OrgID;
                    if (GetOrganizationofLoggedinuser != null)
                    {
                        var GettingPlayersofOrganization = context.Players.Where(a => a.OrgId == GetOrganizationofLoggedinuser && a.PlayerId == PlayerID).ToList();

                        if (GettingPlayersofOrganization.Count > 0)
                        {

                            foreach (var item in GettingPlayersofOrganization)
                            {
                                var ss = context.PlayerExcercises.Where(p => p.PlayerId == item.PlayerId).ToList();
                                if (ss.Count > 0)
                                {
                                    foreach (var item1 in ss)
                                    {
                                        PlayerMeasurement plm = new PlayerMeasurement();
                                        plm.PlayerId = item.PlayerId;
                                        plm.OrgID = item.OrgId;
                                        plm.ExcerciseBatchId = ExcerciseBatchId;
                                        plm.ExcerciseID = item1.ExcerciseId;
                                        plm.ExcerciseValue = 0;
                                        plm.Datetime = DateTime.Now;
                                        context.Entry(plm).State = EntityState.Added;
                                        context.SaveChanges();
                                    }
                                }
                            }
                            return Json(new { success = true, Message = "Measurement has been done for all players" });
                        }
                        else
                        {
                            return Json(new { success = false, Message = "Measurement Is not done." });
                        }
                    }
                }

            }
            return Json(new { success = false, Message = "Error" });
        }


        public ActionResult HierarchyBinding_Orders(int PlayerId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetPlayerMesasurements()
                 .Where(order => order.PlayerId == PlayerId)
                 .ToDataSourceResult(request));
        }

        private void ModifyFilters(IEnumerable<IFilterDescriptor> filters)
        {
            List<string> dt = new List<string>();
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "Datetime")
                    {
                        var datetime = descriptor.ConvertedValue;
                        dt.Add(datetime.ToString());
                        Session["dt"] = dt;

                    }
                    else if (filter is CompositeFilterDescriptor)
                    {

                        ModifyFilters(((CompositeFilterDescriptor)filter).FilterDescriptors);
                    }
                }
            }
        }

        public ActionResult AlertAndRisk()
        {
            // var GetAllPlayerMesuremnts = (from p in context.Players join pm in context.PlayerMeasurements on p.PlayerId equals pm.PlayerId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue }).ToList();
            var AllExcercise = context.Excercises.Where(a => a.Active == true).ToList();
            AllExcercise.Insert(0, new Excercise { ExerciseID = -1, ExerciseName = "AllExcercises", Active = true });
            ViewBag.Excersices = new SelectList(AllExcercise, "ExerciseID", "ExerciseName");
            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, MeasurementBatchViewModel product)
        {
            if (product != null && ModelState.IsValid)
            {
                PlayerMeasurement plm = context.PlayerMeasurements.FirstOrDefault(a => a.Id == product.Id);
                plm.ExcerciseValue = product.ExcerciseValue;
                context.Entry(plm).State = EntityState.Modified;
                context.SaveChanges();
            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Destroy([DataSourceRequest] DataSourceRequest request, MeasurementBatchViewModel product)
        {
            if (product != null)
            {
                PlayerMeasurement plm = context.PlayerMeasurements.FirstOrDefault(a => a.Id == product.Id);
                context.Entry(plm).State = EntityState.Deleted;
                context.SaveChanges();
            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState));
        }

        private static IEnumerable<MeasurementBatchViewModel> GetPlayerMesasurements()
        {
            var northwind = new DbMVC5ProjectEntities();
            var GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseBatchId descending select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName }).ToList().Take(5);
            return GetAllPlayerMesuremnts;
        }


        /// <summary>
        /// 20 October
        /// </summary>
        /// <param name="PlayerId"></param>
        /// <returns></returns>
        private static IEnumerable<MeasurementBatchViewModel> GetPlayerMesasuForRiskProfile(string Sorting, int? orgId, string role, int? ExcerciseBatchId, int? Excesiseid)
        {
            //var user = await UserManager.FindAsync(orgId);

            var northwind = new DbMVC5ProjectEntities();

            IEnumerable<MeasurementBatchViewModel> GetAllPlayerMesuremnts = null;
            if (Sorting == "Descending")
            {
                if (role == "System Admin")
                {
                    if (ExcerciseBatchId == 0 && Excesiseid == 0)
                    {
                        GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                    }
                    else
                    {
                        if (ExcerciseBatchId != 0 && Excesiseid == 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else if (ExcerciseBatchId == 0 && Excesiseid != 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.ExcerciseID == Excesiseid select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                    }
                }
                else
                {
                    if (ExcerciseBatchId == 0 && Excesiseid == 0)
                    {
                        GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.OrgID == orgId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                    }
                    else
                    {
                        if (ExcerciseBatchId != 0 && Excesiseid == 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.OrgID == orgId && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else if (ExcerciseBatchId == 0 && Excesiseid != 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }

                    }

                }
                //if(user.IsInRole("System Admin"))
                //{
                //    GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending select new MeasurementBatchViewModel { PlayerId = p.PlayerId,Id = pm.Id,Name = p.PlayerFirstName + " " + p.PlayerLastName,ExcerciseValue = pm.ExcerciseValue,ExcerciseBatchId = pm.ExcerciseBatchId,ExcerciseID = pm.ExcerciseID,ExcerciseName = e.ExerciseName,View = "View"}).ToList();
                //}
                //else
                //{
                //    GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue descending where pm.OrgID == orgId select new MeasurementBatchViewModel { PlayerId = p.PlayerId,Id = pm.Id,Name = p.PlayerFirstName + " " + p.PlayerLastName,ExcerciseValue = pm.ExcerciseValue,ExcerciseBatchId = pm.ExcerciseBatchId,ExcerciseID = pm.ExcerciseID,ExcerciseName = e.ExerciseName,View = "View"}).ToList();
                //}
            }
            else
            {
                if (role == "System Admin")
                {
                    if (ExcerciseBatchId == 0 && Excesiseid == 0)
                    {
                        GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                    }
                    else
                    {
                        if (ExcerciseBatchId != 0 && Excesiseid == 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else if (ExcerciseBatchId == 0 && Excesiseid != 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.ExcerciseID == Excesiseid select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                    }
                }
                else
                {
                    //if (ExcerciseBatchId == 0 && Excesiseid == 0)
                    //{
                    //    GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                    //}
                    //else
                    //{
                    //    GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                    //}

                    if (ExcerciseBatchId == 0 && Excesiseid == 0)
                    {
                        GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                    }
                    else
                    {
                        if (ExcerciseBatchId != 0 && Excesiseid == 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else if (ExcerciseBatchId == 0 && Excesiseid != 0)
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }
                        else
                        {
                            GetAllPlayerMesuremnts = (from p in northwind.Players join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID orderby pm.ExcerciseValue ascending where pm.OrgID == orgId && pm.ExcerciseID == Excesiseid && pm.ExcerciseBatchId == ExcerciseBatchId select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, ExcerciseBatchId = pm.ExcerciseBatchId, ExcerciseID = pm.ExcerciseID, ExcerciseName = e.ExerciseName, View = "View" }).ToList();
                        }

                    }
                }

            }

            return GetAllPlayerMesuremnts;
        }
        public ActionResult PlayerInfo(int? PlayerId)
        {
            PlayerInfoViewModel model = new PlayerInfoViewModel();
            var playerinfo = context.Players.FirstOrDefault(a => a.PlayerId == PlayerId);
            if (playerinfo != null)
            {
                model.PlayerPicture = playerinfo.PlayerPicture;
                model.PlayerId = playerinfo.PlayerId;
                model.CombineName = playerinfo.PlayerFirstName + " " + playerinfo.PlayerLastName;
                model.PlayerPhoneNumber = playerinfo.PlayerPhoneNumber;
                model.PlayerEmailAddress = playerinfo.PlayerEmailAddress;
                model.DOB = Convert.ToDateTime(playerinfo.DateOfBirth).ToShortDateString();
                model.PlayerNumber = playerinfo.PlayerNumber;
            }
            return View(model);
        }

        public ActionResult DetailOfMeasuremnetByPlayerId([DataSourceRequest] DataSourceRequest request)
        {
            var fdate = "";
            var ldate = "";
            if (request.Filters.Count != 0)
            {
                ModifyFilters(request.Filters);
                dynamic dt = Session["dt"];
                fdate = dt[0];
                ldate = dt[1];

            }
            string QueryStringValue = Request.UrlReferrer.Query;
            int IndexValue = QueryStringValue.LastIndexOf("=");
            QueryStringValue = QueryStringValue.Substring(IndexValue + 1);
            int PlayerId = Convert.ToInt32(QueryStringValue);
            string sortingorder = "";
            if (request.Sorts.Count != 0)
            {
                sortingorder = request.Sorts[0].SortDirection.ToString();
            }
            else
            {
                sortingorder = "Descending";
            }
            //Session["List"] = GetPlayerMesasurementsByPlayerId(PlayerId, sortingorder);

            //return Json(GetPlayerMesasurementsByPlayerId(PlayerId, sortingorder).ToDataSourceResult(request));
            Session["List"] = GetPlayerMesasurementsByPlayerId(PlayerId, sortingorder, fdate, ldate);

            return Json(GetPlayerMesasurementsByPlayerId(PlayerId, sortingorder, fdate, ldate).ToDataSourceResult(request));
        }

        public ActionResult ToolbarTemplate_Categories()
        {

            var dataContext = new DbMVC5ProjectEntities();
            var ssd = (from p in context.ExcerciseBatches orderby p.ExerciseBatchId descending select p).ToList();
            List<ExcerciseBatchViewModel> list = new List<ExcerciseBatchViewModel>();
            foreach (var item in ssd)
            {
                ExcerciseBatchViewModel model = new ExcerciseBatchViewModel();
                string varr = Convert.ToString(item.Datetime);
                model.ExerciseBatchId = item.ExerciseBatchId;
                model.Datetime1 = varr;
                list.Add(model);
            }


            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCombindData()
        {
            var dataContext = new DbMVC5ProjectEntities();
            var ExcerBataches = (from p in context.ExcerciseBatches orderby p.ExerciseBatchId descending select p).ToList();
            var Excercise = context.Excercises.ToList();
            List<CombineDataViewModel> list = new List<CombineDataViewModel>();
            int count = 0;
            foreach (var item1 in ExcerBataches)
            {
                CombineDataViewModel model1 = new CombineDataViewModel();
                model1.Id = count + "-" + item1.ExerciseBatchId;
                model1.value = "All" + "-" + item1.Datetime;
                list.Add(model1);
                foreach (var item in Excercise)
                {
                    count--;
                    CombineDataViewModel model = new CombineDataViewModel();
                    model.Id = item.ExerciseID + "-" + item1.ExerciseBatchId;
                    model.value = item.ExerciseName + "-" + item1.Datetime;
                    count++;

                    list.Add(model);
                    // list.Insert(0, new CombineDataViewModel {Id="0",value="All -"+item1.Datetime });
                }
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ToolbarTemplate_Excerise()
        {

            var dataContext = new DbMVC5ProjectEntities();
            var categories = dataContext.Excercises.Where(e => e.Active == true)
                        .Select(c => new ExcesisesViewModel
                        {
                            ExerciseID = c.ExerciseID,
                            ExerciseName = c.ExerciseName
                        })
                        .OrderBy(e => e.ExerciseID);

            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        /// <returns></returns>
        public void TransformListIntoCsvFile()
        {
            List<MeasurementBatchViewModel> mn = Session["List"] as List<MeasurementBatchViewModel>;
            StringWriter sw = new StringWriter();
            sw.WriteLine("\"Name\",\"OrganizationName\",\"ExcerciseValue\",\"Datetime\"");
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=Exported_customers.csv");
            Response.ContentType = "text/csv";
            foreach (var line in mn)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\"",
                                           line.Name,
                                           line.OrganizationName,
                                           line.ExcerciseValue,
                                           line.Datetime));
            }

            Response.Write(sw.ToString());

            Response.End();

        }


        private static IEnumerable<MeasurementBatchViewModel> GetPlayerMesasurementsByPlayerId(int PlayerId, string sortingorder, string firstdate, string lastdate)
        {
            DateTime fDate = new DateTime();

            DateTime lDate = new DateTime();
            if (firstdate != "")
            {
                fDate = DateTime.Parse(firstdate);
                lDate = DateTime.Parse(lastdate);
            }
            var northwind = new DbMVC5ProjectEntities();
            IEnumerable<MeasurementBatchViewModel> GetAllPlayerMesuremnts = null;
            if (sortingorder == "Descending")
            {
                if (firstdate != "" && lastdate != "")
                {
                    GetAllPlayerMesuremnts = (from p in northwind.Players
                                              join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId
                                              join or in northwind.Organizations on pm.OrgID equals or.OrgId
                                              join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID
                                              where p.PlayerId == PlayerId && pm.Datetime >= fDate && pm.Datetime <= lDate
                                              orderby pm.ExcerciseValue descending
                                              select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, ExcerciseName = e.ExerciseName, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, OrganizationName = or.OrgName, Datetime = pm.Datetime, ExcerciseBatchId = pm.ExcerciseBatchId }).ToList();
                }
                else
                {
                    GetAllPlayerMesuremnts = (from p in northwind.Players
                                              join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId
                                              join or in northwind.Organizations on pm.OrgID equals or.OrgId
                                              join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID
                                              where p.PlayerId == PlayerId
                                              orderby pm.ExcerciseValue descending
                                              select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, ExcerciseName = e.ExerciseName, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, OrganizationName = or.OrgName, Datetime = pm.Datetime, ExcerciseBatchId = pm.ExcerciseBatchId }).ToList();
                }

            }
            else
            {
                GetAllPlayerMesuremnts = (from p in northwind.Players
                                          join pm in northwind.PlayerMeasurements on p.PlayerId equals pm.PlayerId
                                          join or in northwind.Organizations on pm.OrgID equals or.OrgId
                                          join e in northwind.Excercises on pm.ExcerciseID equals e.ExerciseID
                                          where p.PlayerId == PlayerId && pm.Datetime > fDate && pm.Datetime <= lDate
                                          orderby pm.ExcerciseValue ascending
                                          select new MeasurementBatchViewModel { PlayerId = p.PlayerId, Id = pm.Id, ExcerciseName = e.ExerciseName, Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseValue = pm.ExcerciseValue, OrganizationName = or.OrgName, Datetime = pm.Datetime, ExcerciseBatchId = pm.ExcerciseBatchId }).ToList();
            }

            return GetAllPlayerMesuremnts;
        }

        public ActionResult AlertsModule()
        {
            var alertData = (from pm in context.PlayerMeasurements join p in context.Players on pm.PlayerId equals p.PlayerId join eb in context.ExcerciseBatches on pm.ExcerciseBatchId equals eb.ExerciseBatchId where pm.ExcerciseValue == 0 && pm.ExcerciseBatchId == eb.ExerciseBatchId orderby eb.ExerciseBatchId descending select new MeasurementBatchViewModel { Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseID = pm.ExcerciseID, ExcerciseValue = pm.ExcerciseValue, Id = pm.Id }).ToList();
            return View();
        }



        /// <summary>
        /// 20 October
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult AlertRecord([DataSourceRequest] DataSourceRequest request)
        {
            string role = "";
            var loggedinUser = User.Identity.GetUserId();
            int? orgId = 0;
            var GettingOrgnizationId = context.UserDetails.FirstOrDefault(a => a.UserId == loggedinUser).OrgID;
            if (GettingOrgnizationId != null)
            {
                orgId = GettingOrgnizationId;
            }
            if (User.IsInRole("System Admin"))
            {
                role = "System Admin";
            }
            else
            {
                role = "";
            }
            return Json(Alertdat(orgId, role).ToDataSourceResult(request));
        }


        /// <summary>
        /// 20 October
        /// </summary>
        /// <returns></returns>
        /// 
        private static IEnumerable<MeasurementBatchViewModel> Alertdat(int? orgId, string role)
        {

            var context = new DbMVC5ProjectEntities();
            IEnumerable<MeasurementBatchViewModel> GetAllPlayerMesuremnts = null;
            if (role == "System Admin")
            {
                GetAllPlayerMesuremnts = (from pm in context.PlayerMeasurements join p in context.Players on pm.PlayerId equals p.PlayerId join eb in context.ExcerciseBatches on pm.ExcerciseBatchId equals eb.ExerciseBatchId where pm.ExcerciseValue == 0 && pm.ExcerciseBatchId == eb.ExerciseBatchId orderby eb.ExerciseBatchId descending select new MeasurementBatchViewModel { Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseID = pm.ExcerciseID, ExcerciseValue = pm.ExcerciseValue, Id = pm.Id, ExcerciseBatchId = pm.ExcerciseBatchId }).ToList();
            }
            else
            {
                GetAllPlayerMesuremnts = (from pm in context.PlayerMeasurements join p in context.Players on pm.PlayerId equals p.PlayerId join eb in context.ExcerciseBatches on pm.ExcerciseBatchId equals eb.ExerciseBatchId where pm.ExcerciseValue == 0 && pm.ExcerciseBatchId == eb.ExerciseBatchId && pm.OrgID == orgId orderby eb.ExerciseBatchId descending select new MeasurementBatchViewModel { Name = p.PlayerFirstName + " " + p.PlayerLastName, ExcerciseID = pm.ExcerciseID, ExcerciseValue = pm.ExcerciseValue, Id = pm.Id, ExcerciseBatchId = pm.ExcerciseBatchId }).ToList();
            }

            return GetAllPlayerMesuremnts;
        }
        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult ActivityReport()
        {
            return View();
        }
        public ActionResult ActivityReport_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(ActivityReportDatasource().ToDataSourceResult(request));
        }
        private static IEnumerable<ActivityReportViewModel> ActivityReportDatasource()
        {
            var context = new DbMVC5ProjectEntities();
            IEnumerable<ActivityReportViewModel> GetAllPlayerReports = null;


            GetAllPlayerReports = (from pm in context.PlayerMeasurements
                                   join p in context.Players on pm.PlayerId equals p.PlayerId
                                   join e in context.Excercises on pm.ExcerciseID equals e.ExerciseID
                                   select new ActivityReportViewModel
                                       {
                                           Datetime = pm.Datetime,
                                           ExcerciseValue = pm.ExcerciseValue,
                                           FullName = p.PlayerFirstName + " " + p.PlayerLastName,
                                           PlayerId = p.PlayerId,
                                           ExerciseName = e.ExerciseName

                                       }
                                      ).ToList();


            return GetAllPlayerReports;
        }
        public ActionResult ManagePlayers()
        {
            return View();
        }
        public async Task<ActionResult> GettingPlayers([DataSourceRequest] DataSourceRequest request)
        {
            var loogedInuser = User.Identity.GetUserId();
            int? OrganiztionId = 10;
            //var gettingOrganizationid = context.UserDetails.FirstOrDefault(a => a.UserId == loogedInuser).OrgID;
            //if (gettingOrganizationid != null)
            //{
            //    OrganiztionId = context.Players.FirstOrDefault(a => a.OrgId == gettingOrganizationid).OrgId;
            //}

            return Json(await AllPlayers(OrganiztionId),JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_UpdatePlayer([DataSourceRequest] DataSourceRequest request, PlayerInfoViewModel product)
        {
            if (product != null && ModelState.IsValid)
            {
                Player plm = context.Players.FirstOrDefault(a => a.PlayerId == product.PlayerId);
                plm.PlayerFirstName = product.PlayerFirstName;
                plm.PlayerLastName = product.PlayerLastName;
                plm.PlayerPhoneNumber = product.PlayerPhoneNumber;
                plm.PlayerEmailAddress = product.PlayerEmailAddress;
                plm.DateOfBirth = product.DateOfBirth;
                plm.OrgStartDate = product.OrgStartDate;
                context.Entry(plm).State = EntityState.Modified;
                context.SaveChanges();
            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_DestroyPlayer([DataSourceRequest] DataSourceRequest request, PlayerInfoViewModel product)
        {
            if (product != null)
            {
                Player plm = context.Players.FirstOrDefault(a => a.PlayerId == product.PlayerId);
                context.Entry(plm).State = EntityState.Deleted;
                context.SaveChanges();
            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult PlayerExcercises()
        {
            return View();
        }
        public async Task<JsonResult> Employees_Read()
        {
            ApiRequests obj = new ApiRequests();
            var list= await obj.allExercises();
            //var model = context.Excercises;
            //// var ViewModel = new List<ExcesisesViewModel>();
            //List<ExcesisesViewModel> list = new List<ExcesisesViewModel>();
            //foreach (var employee in model)
            //{
            //    ExcesisesViewModel excer = new ExcesisesViewModel();
            //    excer.ExerciseID = employee.ExerciseID;
            //    excer.ExerciseName = employee.ExerciseName;
            //    list.Add(excer);
            //    // ViewModel.Add(new ExcesisesViewModel(employee));
            //}

            return Json(list, JsonRequestBehavior.AllowGet);
        }
        private async static Task<IEnumerable<PlayerInfoViewModel>> AllPlayers(int? orgID)
        {
            ApiRequests obj = new ApiRequests();

            //var context = new DbMVC5ProjectEntities();
            IEnumerable<PlayerInfoViewModel> GetAllPlayer = null;
            string newresponsefromserver=await obj.GetAllTeamPlayer(orgID.ToString());
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerInfoViewModel>>(newresponsefromserver);
            //GetAllPlayer = (from p in context.Players
            //                where p.OrgId == orgID
            //                select new
            //                    PlayerInfoViewModel
            //                    {
            //                        CombineName = p.PlayerFirstName + " " + p.PlayerLastName,
            //                        PlayerId = p.PlayerId,
            //                        PlayerEmailAddress = p.PlayerEmailAddress,
            //                        OrgStartDate = p.OrgStartDate,
            //                        PlayerPhoneNumber = p.PlayerPhoneNumber,
            //                        PlayerPicture = p.PlayerPicture,
            //                        PlayerNumber = p.PlayerNumber,
            //                        PlayerFirstName = p.PlayerFirstName,
            //                        PlayerLastName = p.PlayerLastName,
            //                        DateOfBirth = p.DateOfBirth
            //                    }).ToList();


            return GetAllPlayer;
        }

        
        [HttpPost]
        public async Task<JsonResult> getAllTeamPlayer(string orgId)
        {
            string orgID = "10";
            ApiRequests obj = new ApiRequests();

            //var context = new DbMVC5ProjectEntities();
            IEnumerable<PlayerInfoViewModel> GetAllPlayer = null;
            string newresponsefromserver = await obj.GetAllTeamPlayer(orgID.ToString());
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerInfoViewModel>>(newresponsefromserver);
            return Json(GetAllPlayer, JsonRequestBehavior.AllowGet);
        }
    }
}