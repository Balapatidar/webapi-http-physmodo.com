﻿using MVC5Project.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class GetValIdController : Controller
    {
        //
        // GET: /GetValId/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetValId()
        {
            return View();
        }

        public async Task<String> valId(string id) {
            ApiRequests obj = new ApiRequests();
            string s = await obj.api_Values_Id(id);
            return s;
        }
	}
}