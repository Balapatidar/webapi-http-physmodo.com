﻿using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class GetAzureStorageController : Controller
    {
        //
        // GET: /GetAzureStorage/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAzureStorage()
        {
            return View();
        }
        public async Task<JsonResult> AzureStorage(string storageID)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetAzureStorage(storageID);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
	}
}