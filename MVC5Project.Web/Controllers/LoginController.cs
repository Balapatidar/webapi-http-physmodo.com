﻿using MVC5Project.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        public bool loginApi(string userName, string userPwd)
        {
            bool rtnVal = false;
            rtnVal=AuthToken.connectHTTP(userName, userPwd);
            if(rtnVal==true)
            {
            Session["UserEmail"]=userName;
                Session["UserPwd"]=userPwd;
            }
            return (rtnVal);

           // return Json(AuthToken.physmodoAccessToken);
        }

        public async Task<ActionResult> logout(string userName, string userPwd)
        {
          //  bool rtnVal = false;
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("Email", Convert.ToString(Session["UserEmail"])));
            postData.Add(new KeyValuePair<string, string>("Password", Convert.ToString(Session["UserPwd"])));
            HttpContent content=new FormUrlEncodedContent(postData);
            ApiRequests objApi = new ApiRequests();
         string res=  await objApi.Logout(content);
         if (res == "OK")
         {
             Session["UserEmail"] = null;
             Session["UserPwd"] = null;
             return RedirectToAction("Login", "Login");

         }
         else { //rtnVal = false;
         return RedirectToAction("Login", "Login");
         }
       

            // return Json(AuthToken.physmodoAccessToken);
        }
	}
}