﻿using System.Globalization;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MVC5Project.Domain.Model;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC5Project.Web.Models;
using System.Collections.Generic;
using System.IO;

namespace IdentitySample.Controllers
{
   // [Authorize]
    [OutputCache(Duration = 0, NoStore = true, VaryByParam = "*")]
    public class HomeController : Controller
    {
        DbMVC5ProjectEntities context = new DbMVC5ProjectEntities();
        public HomeController()
        {
        }
        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }
        public ActionResult Index()
        {
            var logeduser = User.Identity.GetUserId();
            UserViewModel u = new UserViewModel();
            UserDetail ud = context.UserDetails.FirstOrDefault(a => a.UserId == logeduser);
            if (ud != null)
            {

                u.combinename = ud.firstname + " " + ud.lastname;
                u.Filepath = ud.picturepath;
                u.OrgID = ud.OrgID;
                if (ud.OrgID != null)
                {
                    var OrganizationName = context.Organizations.FirstOrDefault(a => a.OrgId == ud.OrgID).OrgName;
                    if (OrganizationName != null)
                    {
                        ViewBag.data = "Organization :" + OrganizationName;
                    }
                }
            }
            return View(u);
        }
        #region Organization
        
       
        public ActionResult Organizations()
        {
            string emptydata = "";
            if (emptydata == "")
            {
                RedirectToAction("OrganizationsList");
            }
            return View();
        }
        [HttpPost]
        public JsonResult OrganizationsList(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                string loogedinuser = User.Identity.GetUserId();
                //Get data from database

                var GetAllOrganizations = context.Organizations.Select(a => new OrganizationViewModel { OrgName = a.OrgName, OrgId = a.OrgId, IsActive = a.IsActive }).ToList();



                var records = GetAllOrganizations.Skip(jtStartIndex).Take(jtPageSize).ToList();
                int CountOfrecords = GetAllOrganizations.Count;
                //Return result to jTable
                return Json(new { Result = "OK", Records = records, TotalRecordCount = CountOfrecords });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public async Task<JsonResult> AddOrg(OrganizationViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                Organization org = new Organization();
                org.OrgName = model.OrgName;
                org.IsActive = model.IsActive;
                context.Entry(org).State = EntityState.Added;
                context.SaveChanges();
                // UserDetail addedStudent = context.Entry(student).State = EntityState.Added;
                return Json(new { Result = "OK", Record = org });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public ActionResult EditOrg(OrganizationViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                Organization org = context.Organizations.FirstOrDefault(a => a.OrgId == model.OrgId);
                org.OrgName = model.OrgName;
                if (model.IsActive == null)
                {
                    org.IsActive = false;
                }
                else
                {
                    org.IsActive = model.IsActive;
                }

                context.Entry(org).State = EntityState.Modified;
                context.SaveChanges();
                // UserDetail addedStudent = context.Entry(student).State = EntityState.Added;
                return Json(new { Result = "OK", Record = org });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult DeleteOrg(int OrgId)
        {
            try
            {
                //  _repository.StudentRepository.DeleteStudent(studentId);
                Organization ud = context.Organizations.Find(OrgId);
                if (ud != null)
                {

                    context.Organizations.Remove(ud);
                    context.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        #endregion

        public ActionResult Users()
        {
            string emptydata = "";
            if (emptydata == "")
            {
                RedirectToAction("List");
            }
            return View();
        }

        [HttpPost]
        public JsonResult List(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                string loogedinuser = User.Identity.GetUserId();
                //Get data from database

                var GetAllClientUsers = context.UserDetails.Where(p => p.CreatedByUserId == loogedinuser).ToList();
                List<UserClientViewModel> userclient = new List<UserClientViewModel>();
                foreach (var item in GetAllClientUsers)
                {

                    UserClientViewModel model = new UserClientViewModel();
                    model.ID = item.ID;
                    model.UserId = item.UserId;
                    model.firstname = item.firstname;
                    model.lastname = item.lastname;
                    model.address = item.address;
                    model.city = item.city;
                    model.state = item.state;
                    model.phone = item.phone;
                    model.email = item.email;
                    model.RoleId = item.Roleid;
                    model.IsActive = item.IsActive;
                    model.zip = item.zip;
                    model.primarycontact = item.primarycontact;
                    userclient.Add(model);
                }

                var records = userclient.Skip(jtStartIndex).Take(jtPageSize).ToList();
                int CountOfrecords = userclient.Count;

                //Return result to jTable
                return Json(new { Result = "OK", Records = records, TotalRecordCount = CountOfrecords });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public ActionResult Admin()
        {
            return View();
        }
        public ActionResult AdminiStration()
        {
            return View("SystemAdministration");
        }

        public ActionResult RegisterdUsers()
        {
            string emptydata = "";
            if (emptydata == "")
            {
                RedirectToAction("RequestAceess");
            }
            return View();
        }

        [HttpPost]
        public JsonResult RequestAceess(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                string loogedinuser = User.Identity.GetUserId();
                //Get data from database

                var GetAllClientUsers = context.RequestAccesses.Where(a => a.IsDeleted == false).ToList();
                List<RequestAccessViewModel> userclient = new List<RequestAccessViewModel>();
                foreach (var item in GetAllClientUsers)
                {

                    RequestAccessViewModel model = new RequestAccessViewModel();
                    model.ID = item.ID;
                    model.Email = item.Email;
                    model.OrganizationName = item.OrganizationName;
                    model.UserId = item.UserId;
                    userclient.Add(model);
                }
                var records = userclient.Skip(jtStartIndex).Take(jtPageSize).ToList();
                int CountOfrecords = userclient.Count;
                //Return result to jTable
                return Json(new { Result = "OK", Records = records, TotalRecordCount = CountOfrecords });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Deny(int ID)
        {
            try
            {
                RequestAccess ud = context.RequestAccesses.Find(ID);
                if (ud != null)
                {
                    ud.IsDeleted = true;
                    context.Entry(ud).State = EntityState.Modified;
                    context.SaveChanges();
                    var getuserByid = context.AspNetUsers.FirstOrDefault(a => a.Id == ud.UserId);
                    // var getUserDetail = context.UserDetails.FirstOrDefault(a => a.UserId == ud.UserId);
                    if (getuserByid != null)
                    {
                        var user = await UserManager.FindByIdAsync(getuserByid.Id);
                        var rolesForUser = await UserManager.GetRolesAsync(getuserByid.Id);
                        if (rolesForUser.Count() > 0)
                        {
                            foreach (var item in rolesForUser)
                            {
                                // item should be the name of the role
                                var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                            }
                        }
                        await UserManager.DeleteAsync(user);
                        //if (getUserDetail != null)
                        //{
                        //    context.UserDetails.Remove(getUserDetail);
                        //    context.SaveChanges();
                        //}
                    }

                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Approve(int ID)
        {
            try
            {
                RequestAccess ud = context.RequestAccesses.Find(ID);
                if (ud != null)
                {
                    UserDetail user = new UserDetail();
                    if (user != null)
                    {
                        var IsOrganizationExist = context.Organizations.FirstOrDefault(a => a.OrgName == ud.OrganizationName);
                        if(IsOrganizationExist ==null)
                        {
                            Organization org = new Organization();
                            org.OrgName = ud.OrganizationName;
                            org.IsActive = true;
                            context.Entry(org).State = EntityState.Added;
                            context.SaveChanges();
                            user.email = ud.Email;
                            user.UserId = ud.UserId;
                            user.IsApproved = true;
                            user.Roleid = 1;
                            var IsOrganizationExistAfterAdding = context.Organizations.FirstOrDefault(a => a.OrgName == ud.OrganizationName);
                            if(IsOrganizationExistAfterAdding!=null)
                            {
                                user.OrgID = IsOrganizationExistAfterAdding.OrgId;
                            }
                            context.Entry(user).State = EntityState.Added;
                            context.SaveChanges();
                        }
                        else
                        {
                            user.email = ud.Email;
                            user.UserId = ud.UserId;
                            user.IsApproved = true;
                            user.Roleid = 1;
                            user.OrgID = IsOrganizationExist.OrgId;
                            context.Entry(user).State = EntityState.Added;
                            context.SaveChanges();
                        }
                       
                         var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                         UserManager.AddToRole(ud.UserId, "Client Admin");
                      
                    }
                    ud.IsDeleted = true;
                    context.Entry(ud).State = EntityState.Modified;
                    context.SaveChanges();


                    string To = ud.Email, UserID, Password, SMTPPort, Host;

                    //HTML Template for Send email
                    string subject = "Approval Message";


                    string body1 = "Thank you for registration on our site.<br /><br/><p>You have been approved.</p><br/>";



                    string body = "<p>Thanks </p><br/>";

                    //Get and set the AppSettings using configuration manager.

                    EmailManager.AppSettings(out UserID, out Password, out SMTPPort, out Host);


                    //Call send email methods.
                    bool result = EmailManager.ApprovalEmail(UserID, subject, body1, To, UserID, Password, SMTPPort, Host);
                    if (result == false)
                    {
                        return Json(new { Result = "NOT OK" });
                    }

                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public ActionResult SystemAdminUsers()
        {
            string emptydata = "";
            if (emptydata == "")
            {
                RedirectToAction("SystemuseradminList");
            }
            return View();
        }

        [HttpPost]
        public JsonResult SystemuseradminList(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                string loogedinuser = User.Identity.GetUserId();
                //Get data from database

                var GetAllClientUsers = context.UserDetails.Where(a => a.IsApproved == true).ToList();
                List<UserViewModel> userclient = new List<UserViewModel>();
                foreach (var item in GetAllClientUsers)
                {

                    UserViewModel model = new UserViewModel();
                    model.ID = item.ID;
                    model.UserId = item.UserId;
                    model.firstname = item.firstname;
                    model.lastname = item.lastname;
                    model.address = item.address;
                    model.city = item.city;
                    model.state = item.state;
                    model.phone = item.phone;
                    model.email = item.email;
                    model.RoleId = item.Roleid;
                    model.OrgID = item.OrgID;
                    model.primarycontact = item.primarycontact;
                    model.zip = item.zip;
                    //  model.IsActive = item.IsActive;
                    userclient.Add(model);
                }
                var records = userclient.Skip(jtStartIndex).Take(jtPageSize).ToList();
                int CountOfrecords = userclient.Count;
                //Return result to jTable
                return Json(new { Result = "OK", Records = records, TotalRecordCount = CountOfrecords });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Saving The user created by system Admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> AddClient(UserViewModel model)
        {
            try
            {
                var loggedinUser = User.Identity.GetUserId();
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                UserDetail ud = new UserDetail();


                var user = new ApplicationUser() { UserName = model.email, Email = model.email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {

                    ud.email = model.email;
                    ud.UserId = user.Id;
                    ud.OrgID = model.OrgID;
                    ud.firstname = model.firstname;
                    ud.lastname = model.lastname;
                    ud.phone = model.phone;
                    ud.primarycontact = model.primarycontact;
                    ud.zip = model.zip;
                    ud.address = model.address;
                    ud.city = model.city;
                    ud.state = model.state;
                    ud.Roleid = model.RoleId;
                    ud.CreatedByUserId = loggedinUser;
                    ud.IsApproved = true;
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                    switch (model.RoleId)
                    {
                        case 1:
                            if (roleManager.RoleExists("Client Admin") == false)
                            {
                                roleManager.Create(new IdentityRole("Client Admin"));
                                UserManager.AddToRole(user.Id, "Client Admin");
                            }
                            else
                            {
                                UserManager.AddToRole(user.Id, "Client Admin");
                            }

                            break;
                        case 2:
                            if (roleManager.RoleExists("client standard user") == false)
                            {
                                roleManager.Create(new IdentityRole("client standard user"));
                                UserManager.AddToRole(user.Id, "client standard user");
                            }
                            else
                            {
                                UserManager.AddToRole(user.Id, "client standard user");
                            }
                            break;
                    }

                    context.Entry(ud).State = EntityState.Added;
                    context.SaveChanges();
                    return Json(new { Result = "OK", Record = ud });
                }
                else
                {
                    return Json(new { Result = "ERROR", Message = result.Errors });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Updating the user By System Admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditClient(UserViewModel model)
        {
            try
            {
                var loggedinUser = User.Identity.GetUserId();
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                UserDetail ud = context.UserDetails.FirstOrDefault(a => a.ID == model.ID);


                ud.OrgID = model.OrgID;
                ud.firstname = model.firstname;
                ud.lastname = model.lastname;
                ud.phone = model.phone;
                ud.primarycontact = model.primarycontact;
                ud.zip = model.zip;
                ud.address = model.address;
                ud.city = model.city;
                ud.state = model.state;
                //ud.CreatedByUserId = loggedinUser;
                ud.Roleid = model.RoleId;
                var getuserid = context.UserDetails.FirstOrDefault(a => a.ID == model.ID).UserId;
                var GetRoles = UserManager.GetRoles(getuserid);
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                switch (model.RoleId)
                {
                    case 1:
                        if (roleManager.RoleExists("Client Admin") == false)
                        {
                            foreach (var item in GetRoles)
                            {
                                if (item == "client standard user")
                                {
                                    UserManager.RemoveFromRole(getuserid, "client standard user");
                                    UserManager.AddToRole(getuserid, "Client Admin");
                                }
                            }
                            roleManager.Create(new IdentityRole("Client Admin"));
                            // UserManager.AddToRole(.Id, "Client Admin");
                        }
                        else
                        {
                            if (GetRoles.Count > 0)
                            {
                                foreach (var item in GetRoles)
                                {
                                    if (item == "client standard user")
                                    {
                                        UserManager.RemoveFromRole(getuserid, "client standard user");
                                        UserManager.AddToRole(getuserid, "Client Admin");
                                    }
                                }
                            }
                            else
                            {
                                UserManager.AddToRole(getuserid, "Client Admin");
                            }
                            //UserManager.AddToRole(user.Id, "Client Admin");
                        }

                        break;
                    case 2:
                        if (roleManager.RoleExists("client standard user") == false)
                        {
                            foreach (var item in GetRoles)
                            {
                                if (item == "Client Admin")
                                {
                                    UserManager.RemoveFromRole(getuserid, "Client Admin");
                                    UserManager.AddToRole(getuserid, "client standard user");
                                }
                            }
                            roleManager.Create(new IdentityRole("client standard user"));
                            // UserManager.AddToRole(user.Id, "client standard user");
                        }
                        else
                        {
                            if (GetRoles.Count > 0)
                            {
                                foreach (var item in GetRoles)
                                {
                                    if (item == "Client Admin")
                                    {
                                        UserManager.RemoveFromRole(getuserid, "Client Admin");
                                        UserManager.AddToRole(getuserid, "client standard user");
                                    }
                                }
                            }
                            else
                            {
                                UserManager.AddToRole(getuserid, "client standard user");
                            }

                        }
                        break;
                }
                context.Entry(ud).State = EntityState.Modified;
                context.SaveChanges();
                // UserDetail addedStudent = context.Entry(student).State = EntityState.Added;
                return Json(new { Result = "OK", Record = ud });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Deleting the user
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> DeleteClient(int Id)
        {
            try
            {
                //  _repository.StudentRepository.DeleteStudent(studentId);
                UserDetail ud = context.UserDetails.Find(Id);
                if (ud != null)
                {

                    context.UserDetails.Remove(ud);
                    context.SaveChanges();
                    var getuserByid = context.AspNetUsers.FirstOrDefault(a => a.Id == ud.UserId);
                    if (getuserByid != null)
                    {
                        var user = await UserManager.FindByIdAsync(getuserByid.Id);
                        var rolesForUser = await UserManager.GetRolesAsync(getuserByid.Id);
                        if (rolesForUser.Count() > 0)
                        {
                            foreach (var item in rolesForUser)
                            {
                                // item should be the name of the role
                                var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                            }
                        }
                        await UserManager.DeleteAsync(user);
                    }
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Action Result for creating New User By Client Admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> Createuser(UserClientViewModel model)
        {
            try
            {
                var loggedinUser = User.Identity.GetUserId();
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                UserDetail ud = new UserDetail();
                //Getting Organiastion Id of Logged in user
                var OrganizationId = context.UserDetails.FirstOrDefault(a => a.UserId == loggedinUser);
                int? OrgId = 0;
                if (OrganizationId != null)
                {
                    OrgId = OrganizationId.OrgID;
                }
                var user = new ApplicationUser() { UserName = model.email, Email = model.email,EmailConfirmed=true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {

                    ud.email = model.email;
                    ud.UserId = user.Id;
                    ud.firstname = model.firstname;
                    ud.lastname = model.lastname;
                    ud.phone = model.phone;
                    ud.primarycontact = model.primarycontact;
                    ud.zip = model.zip;
                    ud.address = model.address;
                    ud.city = model.city;
                    ud.state = model.state;
                    ud.CreatedByUserId = loggedinUser;
                    ud.Roleid = model.RoleId;
                    ud.OrgID = OrgId;
                    ud.IsApproved = true;
                    if (model.IsActive == null)
                    {
                        ud.IsActive = false;
                    }
                    else
                    {
                        ud.IsActive = true;
                    }
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                    switch (model.RoleId)
                    {
                        case 1:
                            if (roleManager.RoleExists("Client Admin") == false)
                            {
                                roleManager.Create(new IdentityRole("Client Admin"));
                                UserManager.AddToRole(user.Id, "Client Admin");
                            }
                            else
                            {
                                UserManager.AddToRole(user.Id, "Client Admin");
                            }
                            break;
                        case 2:
                            if (roleManager.RoleExists("client standard user") == false)
                            {
                                roleManager.Create(new IdentityRole("client standard user"));
                                UserManager.AddToRole(user.Id, "client standard user");
                            }
                            else
                            {
                                UserManager.AddToRole(user.Id, "client standard user");
                            }
                            break;

                    }
                    context.Entry(ud).State = EntityState.Added;
                    context.SaveChanges();
                    return Json(new { Result = "OK", Record = ud });
                }
                else
                {
                    return Json(new { Result = "ERROR", Message = result.Errors });
                }

                // UserDetail addedStudent = context.Entry(student).State = EntityState.Added;

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// upadating the client Admin user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        public JsonResult Updateuser(UserClientViewModel model)
        {
            try
            {
                var loggedinUser = User.Identity.GetUserId();
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                UserDetail ud = context.UserDetails.FirstOrDefault(a => a.ID == model.ID);

                ud.firstname = model.firstname;
                ud.lastname = model.lastname;
                ud.phone = model.phone;
                ud.primarycontact = model.primarycontact;
                ud.zip = model.zip;
                ud.address = model.address;
                ud.city = model.city;
                ud.state = model.state;
                ud.ID = model.ID;
                ud.Roleid = model.RoleId;
                if (model.IsActive == null)
                {
                    ud.IsActive = false;
                }
                else
                {
                    ud.IsActive = true;
                }
                var getuserid = context.UserDetails.FirstOrDefault(a => a.ID == model.ID).UserId;
                var GetRoles = UserManager.GetRoles(getuserid);
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                switch (model.RoleId)
                {
                    case 1:
                        if (roleManager.RoleExists("Client Admin") == false)
                        {
                            foreach (var item in GetRoles)
                            {
                                if (item == "client standard user")
                                {
                                    UserManager.RemoveFromRole(getuserid, "client standard user");
                                    UserManager.AddToRole(getuserid, "Client Admin");
                                }
                            }
                            roleManager.Create(new IdentityRole("Client Admin"));
                            // UserManager.AddToRole(.Id, "Client Admin");
                        }
                        else
                        {
                            if (GetRoles.Count > 0)
                            {
                                foreach (var item in GetRoles)
                                {
                                    if (item == "client standard user")
                                    {
                                        UserManager.RemoveFromRole(getuserid, "client standard user");
                                        UserManager.AddToRole(getuserid, "Client Admin");
                                    }
                                }
                            }
                            else
                            {
                                UserManager.AddToRole(getuserid, "Client Admin");
                            }
                            //UserManager.AddToRole(user.Id, "Client Admin");
                        }

                        break;
                    case 2:
                        if (roleManager.RoleExists("client standard user") == false)
                        {
                            foreach (var item in GetRoles)
                            {
                                if (item == "Client Admin")
                                {
                                    UserManager.RemoveFromRole(getuserid, "Client Admin");
                                    UserManager.AddToRole(getuserid, "client standard user");
                                }
                            }
                            roleManager.Create(new IdentityRole("client standard user"));
                            // UserManager.AddToRole(user.Id, "client standard user");
                        }
                        else
                        {
                            if (GetRoles.Count > 0)
                            {
                                foreach (var item in GetRoles)
                                {
                                    if (item == "Client Admin")
                                    {
                                        UserManager.RemoveFromRole(getuserid, "Client Admin");
                                        UserManager.AddToRole(getuserid, "client standard user");
                                    }
                                }
                            }
                            else
                            {
                                UserManager.AddToRole(getuserid, "client standard user");
                            }
                        }
                        break;
                }
                context.Entry(ud).State = EntityState.Modified;
                context.SaveChanges();

                return Json(new { Result = "OK", Record = ud });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Deleting the client User
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> Deleteuser(int Id)
        {
            try
            {
                //  _repository.StudentRepository.DeleteStudent(studentId);
                UserDetail ud = context.UserDetails.Find(Id);
                if (ud != null)
                {

                    context.UserDetails.Remove(ud);
                    context.SaveChanges();
                    var getuserByid = context.AspNetUsers.FirstOrDefault(a => a.Id == ud.UserId);
                    if (getuserByid != null)
                    {
                        var user = await UserManager.FindByIdAsync(getuserByid.Id);
                        var rolesForUser = await UserManager.GetRolesAsync(getuserByid.Id);
                        if (rolesForUser.Count() > 0)
                        {
                            foreach (var item in rolesForUser)
                            {
                                // item should be the name of the role
                                var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                            }
                        }
                        await UserManager.DeleteAsync(user);
                    }
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public ActionResult AddNewUser()
        {
            ViewBag.roles = LoadRoles();

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> AddNewUser(UserViewModel model)
        {
            var loggedinUser = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.email, Email = model.email,EmailConfirmed=true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {

                    UserDetail ud = new UserDetail();
                    ud.email = model.email;
                    ud.UserId = user.Id;
                    ud.OrgID = model.OrgID;
                    ud.firstname = model.firstname;
                    ud.lastname = model.lastname;
                    ud.phone = model.phone;
                    ud.primarycontact = model.primarycontact;
                    ud.zip = model.zip;
                    ud.address = model.address;
                    ud.city = model.city;
                    ud.state = model.state;
                    ud.IsApproved = true;
                    ud.CreatedByUserId = loggedinUser;
                    context.Entry(ud).State = EntityState.Added;
                    context.SaveChanges();

                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                    switch (model.RoleId)
                    {
                        case 1:
                            if (roleManager.RoleExists("Client Admin") == false)
                            {
                                roleManager.Create(new IdentityRole("Client Admin"));
                                UserManager.AddToRole(user.Id, "Client Admin");
                            }
                            else
                            {
                                UserManager.AddToRole(user.Id, "Client Admin");
                            }

                            break;
                        case 2:
                            if (roleManager.RoleExists("client standard user") == false)
                            {
                                roleManager.Create(new IdentityRole("client standard user"));
                                UserManager.AddToRole(user.Id, "client standard user");
                            }
                            else
                            {
                                UserManager.AddToRole(user.Id, "client standard user");
                            }
                            break;
                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            ViewBag.roles = LoadRoles();
            ViewBag.organization = LoadOrganization(-1);
            return View(model);
        }

        public ActionResult EditProfile()
        {
            var user = User.Identity.GetUserId();
            UserDetail getuserById = null;
            ProfileViewModel prf = new ProfileViewModel();
            if (user != null)
            {
                getuserById = context.UserDetails.FirstOrDefault(a => a.UserId == user);
                if (getuserById != null)
                {
                    prf.firstname = getuserById.firstname;
                    prf.lastname = getuserById.lastname;
                    prf.address = getuserById.address;
                    prf.city = getuserById.city;
                    prf.state = Convert.ToInt32(getuserById.state);
                    prf.zip = getuserById.zip;
                    prf.primarycontact = getuserById.primarycontact;
                    prf.phone = getuserById.phone;
                    prf.UserId = getuserById.UserId;
                    prf.ID = getuserById.ID;

                }
                ViewBag.States = LoadStates(Convert.ToInt32(getuserById.state));
            }

            return View(prf);
        }
        [HttpPost]
        public ActionResult EditProfile(ProfileViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                string targetPath = "";
                UserDetail prf = context.UserDetails.FirstOrDefault(a => a.UserId == model.UserId);
                prf.firstname = model.firstname;
                prf.lastname = model.lastname;
                prf.address = model.address;
                prf.city = model.city;
                prf.state = Convert.ToString(model.state);
                prf.zip = model.zip;
                prf.primarycontact = model.primarycontact;
                prf.phone = model.phone;
                prf.ID = model.ID;
                if (file != null)
                {
                    var fileName = file.FileName;
                    var targetFolder = "";
                    bool folder_exists = Directory.Exists(Server.MapPath("~/uploads"));
                    if (!folder_exists)
                        Directory.CreateDirectory(Server.MapPath("~/uploads"));
                    bool folder_exists2 = Directory.Exists(Server.MapPath("~/uploads/picture"));
                    if (!folder_exists2)
                    {
                        Directory.CreateDirectory(Server.MapPath("~/uploads/picture"));
                    }

                    targetFolder = Server.MapPath("~/uploads/picture");
                    targetPath = Path.Combine(targetFolder, fileName);
                    file.SaveAs(targetPath);
                    prf.picturepath = fileName;
                    context.Entry(prf).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    prf.picturepath = null;
                    context.Entry(prf).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.States = LoadStates(-1);
            return View(model);
        }
        public ActionResult NewClient()
        {
            ViewBag.organization = LoadOrganization(-1);
            ViewBag.roles = LoadRoles();
            ViewBag.states = LoadStates(-1);
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> NewClient(ClientViewModel model)
        {
            var loogedInuser = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.email, Email = model.email,EmailConfirmed=true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {

                    UserDetail ud = new UserDetail();
                    ud.email = model.email;
                    ud.UserId = user.Id;
                    ud.OrgID = model.OrgID;
                    ud.firstname = model.firstname;
                    ud.lastname = model.lastname;
                    ud.phone = model.phone;
                    ud.primarycontact = model.primarycontact;
                    ud.zip = model.zip;
                    ud.address = model.address;
                    ud.city = model.city;
                    ud.state = Convert.ToString(model.state);
                    ud.CreatedByUserId = loogedInuser;
                    ud.Roleid = 1;
                    ud.IsApproved = true;
                    context.Entry(ud).State = EntityState.Added;
                    context.SaveChanges();
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                    if (roleManager.RoleExists("Client Admin"))
                    {
                        UserManager.AddToRole(user.Id, "Client Admin");
                    }
                    else
                    {
                        roleManager.Create(new IdentityRole("Client Admin"));
                        UserManager.AddToRole(user.Id, "Client Admin");
                    }


                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }
            ViewBag.organization = LoadOrganization(-1);
            ViewBag.states = LoadStates(-1);
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }
        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        private List<SelectListItem> LoadOrganization(int? id)
        {
            var Organizations = context.Organizations.Where(a => a.IsActive == true).ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in Organizations)
            {
                if (id == item.OrgId)
                {
                    SelectListItem slt = new SelectListItem();
                    slt.Text = item.OrgName;
                    slt.Value = Convert.ToString(item.OrgId);
                    slt.Selected = true;
                    list.Add(slt);
                }
                else
                {
                    SelectListItem slt = new SelectListItem();
                    slt.Text = item.OrgName;
                    slt.Value = Convert.ToString(item.OrgId);
                    slt.Selected = false;
                    list.Add(slt);
                }
            }
            return list;
        }

        private List<SelectListItem> LoadStates(int? id)
        {
            var StateList = context.States.ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var item in StateList)
            {
                SelectListItem slt = new SelectListItem();
                if (id == item.StateId)
                {

                    slt.Text = item.StateName;
                    slt.Value = Convert.ToString(item.StateId);
                    slt.Selected = true;
                }
                else
                {

                    slt.Text = item.StateName;
                    slt.Value = Convert.ToString(item.StateId);
                    slt.Selected = false;
                }
                list.Add(slt);
            }
            return list;
        }
        [HttpPost]
        public JsonResult GetOrganizations()
        {
            try
            {
                var Organizations = context.Organizations.Where(a => a.IsActive == true).Select(c => new { DisplayText = c.OrgName, Value = c.OrgId }).ToList();


                return Json(new { Result = "OK", Options = Organizations });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetStates()
        {
            try
            {
                var States = context.States.Select(c => new { DisplayText = c.StateName, Value = c.StateId }).ToList();


                return Json(new { Result = "OK", Options = States });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetRoles()
        {
            try
            {
                var Roles = LoadRoles();
                List<RolesList> list = new List<RolesList>();

                // var Rolelist =context.AspNetRoles.Select(c => new { DisplayText = c.Name, Value = c.Id }).ToList();
                var Rolelist = context.AspNetRoles.ToList();
                foreach (var item in Rolelist)
                {
                    RolesList model = new RolesList();
                    if (item.Id == "b0342919-3052-42e8-bde4-a215eecf54ae")
                    {
                        model.Value = 1;
                        model.DisplayText = item.Name;
                        list.Add(model);
                    }
                    else if (item.Id == "8dfe4a80-38da-4405-bcbd-d9b4c1c4e8d9")
                    {
                        model.Value = 2;
                        model.DisplayText = item.Name;
                        list.Add(model);
                    }

                }
                return Json(new { Result = "OK", Options = list });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult GetRolesForAdmin()
        {
            try
            {
                var Roles = LoadRoles();
                List<RolesList> list = new List<RolesList>();

                // var Rolelist =context.AspNetRoles.Select(c => new { DisplayText = c.Name, Value = c.Id }).ToList();
                var Rolelist = context.AspNetRoles.ToList();
                foreach (var item in Rolelist)
                {
                    RolesList model = new RolesList();
                    if (item.Id == "b0342919-3052-42e8-bde4-a215eecf54ae")
                    {
                        model.Value = 1;
                        model.DisplayText = item.Name;
                        list.Add(model);
                    }
                    else if (item.Id == "8dfe4a80-38da-4405-bcbd-d9b4c1c4e8d9")
                    {
                        model.Value = 2;
                        model.DisplayText = item.Name;
                        list.Add(model);
                    }


                }
                return Json(new { Result = "OK", Options = list });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        private List<SelectListItem> LoadRoles()
        {

            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Client Admin", Value = "1" });
            list.Add(new SelectListItem { Text = "client standard user", Value = "2" });
            return list;
        }
        #endregion
    }
}
