﻿using System.Globalization;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MVC5Project.Domain.Model;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC5Project.Web.Models;
using System.Collections.Generic;
using System.IO;
using System.Data.Entity.Validation;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Kendo.Mvc;
using System.Net;
using System.Web.Script.Serialization;
using MVC5Project.Web.Code;
using System.Net.Http;

namespace MVC5Project.Web.Controllers
{
   // [Authorize]
    [OutputCache(Duration = 0, NoStore = true, VaryByParam = "*")]
    public class APIController : Controller
    {
        DbMVC5ProjectEntities context = new DbMVC5ProjectEntities();
        public APIController()
        {
        }
        public APIController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }
        // GET: API
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> MeasurementsRead([DataSourceRequest]DataSourceRequest request)
        {
            var loogedinuser = User.Identity.GetUserId();
            int? getorgid = 0;
            if(loogedinuser !=null)
            {
                 getorgid = context.UserDetails.FirstOrDefault(u => u.UserId == loogedinuser).OrgID;
            }
            string token = Convert.ToString(Session["access_token"]);
            
            int newBatchId = 0;
            int newexcerciseid = 0;
            if(request.Filters.Count>0)
            {
                var filterdata = ModifyFilters(request.Filters);

                if (filterdata != null)
                {
                    if (filterdata.ExcerciseBatchId > 0)
                    {
                        newBatchId = filterdata.ExcerciseBatchId;
                    }
                    else
                    {
                        newBatchId = 0;
                    }
                    if (filterdata.ExcerciseID > 0)
                    {
                        newexcerciseid = filterdata.ExcerciseID;
                    }
                    else
                    {
                        newexcerciseid = 0;
                    }
                }
            }
            //string resp = await GetPlayerMesasurement(getorgid, token, newexcerciseid, newBatchId);
           // ModifyFiltersForCustomers(request.Filters);
            return Json(await GetPlayerMesasurement(getorgid, token, newexcerciseid, newBatchId));
        }
        private async Task<IEnumerable<MeasurementBatchViewModel>> GetPlayerMesasurement(int? orgid, string token,int excerciseid,int batchid)
        {
            ApiRequests obj = new ApiRequests();
            IEnumerable<MeasurementBatchViewModel> GetAllPlayerMesuremnts = null;
            string newresponsefromserver = await obj.playerMesurements(Convert.ToString(orgid), Convert.ToString(excerciseid), Convert.ToString(batchid));
            GetAllPlayerMesuremnts = new JavaScriptSerializer().Deserialize<List<MeasurementBatchViewModel>>(newresponsefromserver);
            return GetAllPlayerMesuremnts;
        }
        public FilterViewModel ModifyFilters(IEnumerable<IFilterDescriptor> filters)
        {
            FilterViewModel filtermodel = new FilterViewModel();
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "ExcerciseID")
                    {
                        filtermodel.ExcerciseID = Convert.ToInt32(descriptor.Value);

                    }
                    if (descriptor != null && descriptor.Member == "ExcerciseBatchId")
                    {
                        filtermodel.ExcerciseBatchId = Convert.ToInt32(descriptor.Value);


                    }
                    else if (filter is CompositeFilterDescriptor)
                    {

                        ModifyFilters(((CompositeFilterDescriptor)filter).FilterDescriptors);
                    }
                }
                return filtermodel;
            }
            return filtermodel;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditingPopup_Update([DataSourceRequest] DataSourceRequest request, MeasurementBatchViewModel product)
        {
            if (product != null && ModelState.IsValid)
            {
                //PlayerMeasurement plm = context.PlayerMeasurements.FirstOrDefault(a => a.Id == product.Id);
                //plm.ExcerciseValue = product.ExcerciseValue;
                //context.Entry(plm).State = EntityState.Modified;
                //context.SaveChanges();
                ApiRequests obj = new ApiRequests();
                string token = Convert.ToString(Session["access_token"]);
                var GettingAllBooks = new JavaScriptSerializer().Serialize(product);
               // HttpContent content = new FormUrlEncodedContent(GettingAllBooks);
                // + "&book=" + book
                string url = "http://api-extranet-physmodo.azurewebsites.net/api/Data?id=" + product.Id + "&book=" + GettingAllBooks;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + token);


                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = GettingAllBooks;// Need to put data here to pass to the API.**

                    streamWriter.Write(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();

                }

               // var responseText =await obj.PutData(product.Id,

            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState));
        }

        public  async Task<ActionResult> GetAllExcercise()
        {
            ApiRequests obj = new ApiRequests();
            string token = Convert.ToString(Session["access_token"]);           
            string newresponsefromserver = await obj.allExercises();
            var GettingAllexcercise = new JavaScriptSerializer().Deserialize<List<ExcesisesViewModel>>(newresponsefromserver);
            List<Excercise> listexcercise = new List<Excercise>();
            var Finallist = GettingAllexcercise.Select(c => new ExcesisesViewModel { ExerciseID = c.ExerciseID, ExerciseName = c.ExerciseName }).OrderBy(e => e.ExerciseID);
            return Json(Finallist, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GettingExcerciseBatches()
        {
            ApiRequests obj = new ApiRequests();
            string token = Convert.ToString(Session["access_token"]);            
            string responseFromServer = await obj.ExercisesBatches();
            var GettingAllExcerciseBatches = new JavaScriptSerializer().Deserialize<List<ExcerciseBatchViewModel>>(responseFromServer);
            List<ExcerciseBatchViewModel> listExcerciseBatches = new List<ExcerciseBatchViewModel>();
            foreach (var item in GettingAllExcerciseBatches)
            {
                ExcerciseBatchViewModel Excercisebatch = new ExcerciseBatchViewModel();
                string varr = Convert.ToString(item.Datetime);
                Excercisebatch.Datetime1 = varr;
                Excercisebatch.ExerciseBatchId = item.ExerciseBatchId;
                listExcerciseBatches.Add(Excercisebatch);
            }
            return Json(listExcerciseBatches, JsonRequestBehavior.AllowGet);
        }
    }
}