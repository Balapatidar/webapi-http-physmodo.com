﻿using MVC5Project.Domain.Model;
using MVC5Project.Web.Code;
using MVC5Project.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MVC5Project.Web.Controllers
{
    public class GetPlayerExercisesController : Controller
    {
        //
        // GET: /GetPlayerExercises/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PlayerExercises()
        {
            return View();
        }

        public async Task<JsonResult> GetPlayerExercises(string playerId)
        {
             ApiRequests obj = new ApiRequests();           
            IEnumerable<PlayerExcercise> GetAllPlayer = null;
            string newresponsefromserver = await obj.GetPlayerExercises(playerId);
            GetAllPlayer = new JavaScriptSerializer().Deserialize<List<PlayerExcercise>>(newresponsefromserver);
            return Json(GetAllPlayer, JsonRequestBehavior.AllowGet);

         

        }
	}
}