﻿using MVC5Project.Web.Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC5Project.Web.Controllers
{
    public class GetOrganizationNameController : Controller
    {
        //
        // GET: /GetOrganizationName/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetOrganizationName()
        {
            return View();
        }

        public async Task<JsonResult> OrgName(string orgID)
        {
            ApiRequests obj = new ApiRequests();
            string json = await obj.GetOrganizationName(orgID);
            var tmp = JsonConvert.DeserializeObject(json);
            return Json(tmp, JsonRequestBehavior.AllowGet);
        }
	}
}